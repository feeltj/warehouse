<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $fillable = ['id', 'name'];
    public function categories(){
        return $this->belongsTo('App\Category');
    }
    public function equiptments(){
        return $this->hasMany('App\Equiptment');
    }
    public function finalcategories()
    {
        return $this->hasMany('App\Finalcategory');
    }
}
