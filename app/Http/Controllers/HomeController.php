<?php

namespace App\Http\Controllers;

use App\User;
use App\Equiptment;
use App\Equiptstatus;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id','=',auth()->user()->id)->get();
        //dd($users);
        $equiptments = Equiptment::all();
        $equiptstatuses = Equiptstatus::all();
        foreach ($users as $user) {
            foreach ($equiptments as $equiptment) {
                foreach ($equiptstatuses as $equiptstatus) {
                    $stock_amount = DB::table('stocks')
                        ->select(DB::raw('COUNT(stocks.equiptment_id) as equiptment_id, user_id'), 'equiptments.name')
                        ->where('user_id', '<>', $user->id)
                        ->orWhere('equiptment_id', '<>', $equiptment->id)
                        ->leftjoin('equiptments', 'equiptments.id', '=', 'stocks.equiptment_id')->groupBy('name', 'user_id')->get();

                    $stock_s = DB::table('stocks')
                        ->select(DB::raw('COUNT(stocks.equiptment_id) as equiptment_id, equiptstatus_id, user_id'), 'equiptstatuses.name')
                        ->where('user_id', '=', $user->id)
                        ->orWhere('equiptment_id', '<>', $equiptment->id)
                        ->orWhere('equiptstatus_id', '<>', $equiptstatus->id)
                        //->where('amount', '=', 1)
                        ->leftjoin('equiptstatuses', 'equiptstatuses.id', '=', 'stocks.equiptstatus_id')->groupBy('name', 'equiptstatus_id','user_id')->get();
                     //dd($stock_s);
                    return view('home', [
                        'users' => $users,
                        'stock_amount' => $stock_amount,
                        'stock_s'=>$stock_s

                    ]);
                }
            }
        }
    }
    public function contacts(){
        return view('contacts');
    }
    public function tickets(){
        return view('tickets');
    }
    public function profile(){
        return view('profile');
    }
    public function forms(){
        return view('forms');
    }
    public function tables(){
        return view('tables');
    }
    public function buttons(){
        return view('buttons');
    }
    public function balance(){
        return view('balance');
    }
    public function passages()
    {
        return view('passages');
    }
    public function settings()
    {
        return view('settings');
    }

}
