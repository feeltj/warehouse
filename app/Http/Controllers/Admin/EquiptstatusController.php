<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Equiptstatus;
class EquiptstatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $equiptstatuses = Equiptstatus::orderBy('id', 'asc')->get();
        $user = \App\User::count();
        $equiptment = \App\Equiptment::count();
        $category = \App\Category::count();
        $warehouse = \App\Warehouse::count();
        return view('admin.equiptstatuses.index',[
           'equiptstatuses'=>$equiptstatuses,
            'equiptment'=>$equiptment,
            'user'=>$user,
            'category'=>$category,
            'warehouse'=>$warehouse
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.equiptstatuses.create',[
            'equiptstatus'=> []
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|max:40'
        ]);
        $equiptstatus = new Equiptstatus();
        $equiptstatus->name = $request->name;
        $equiptstatus->save();
        return redirect()->route('equiptstatuses.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $equiptstatus = Equiptstatus::findOrFail($id);
        return view('admin.equiptstatuses.show',compact('equiptstatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equiptstatus = Equiptstatus::findOrFail($id);
        return view('admin.equiptstatuses.edit',compact('equiptstatus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $equiptstatus = Equiptstatus::findOrFail($id);
        $this->validate($request,[
            'name'=>'required|max:40'
        ]);
        $equiptstatus->name = $request->name;
        $equiptstatus->save();
        return redirect()->route('equiptstatuses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $equiptstatus = Equiptstatus::findOrFail($id);
        $equiptstatus->delete();
        return redirect()->route('equiptstatuses.index');
    }
}
