<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;
use Illuminate\Support\Facades\Hash;
use App\Region;
use App\Department;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    //public function __construct() {
      //  $this->middleware(['auth', 'isAdmin']);//isAdmin middleware lets only users with a //specific permission permission to access these resources
   // }

    public function index() {
        //Get all users and pass it to the view
        //$users = User::all();
        $user = User::count();
        $equiptment = \App\Equiptment::count();
        $category =\App\Category::count();
        $warehouse = \App\Warehouse::count();
        return view('admin.users.index',[
            'users'=> User::paginate(10),
            'user'=>$user,
            'equiptment'=>$equiptment,
            'category'=>$category,
            'warehouse'=>$warehouse
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //Get all roles and pass it to the view
        $roles = Role::get();
        $regions = Region::all();
        $departments = Department::all();
        return view('admin.users.create',[
            'user'=>[],
            'roles'=>$roles,
            'regions'=>$regions,
            'departments'=>$departments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //Validate name, email and password fields
        $validator = $request->validate( [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|integer|digits:9',
            'address' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'region_id'=>$request['region_id'],
            'department_id'=>$request['department_id'],
            'address'=>$request['address'],
            'password' => Hash::make($request['password']),
        ]);


        $roles = $request['roles']; //Retrieving the roles field
        //Checking if a role was selected
        if (isset($roles)) {

            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();
                $user->assignRole($role_r); //Assigning role to user
            }
        }
        //Redirect to the users.index view and display message
        return redirect()->route('users.index')
            ->with('flash_message',
                'User successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = User::find($id); //Get user with specified id
        $roles = Role::get(); //Get all roles
        $regions = Region::all();
        $departments = Department::all();
        return view('admin.users.show',[
            'user'=>$user,
            'roles'=>$roles,
            'regions'=>$regions,
            'departments'=>$departments
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = User::findOrFail($id); //Get user with specified id
        $roles = Role::get(); //Get all roles
        $regions = Region::all();
        $departments = Department::all();
        return view('admin.users.edit', [
            'user'=>$user,
            'roles'=>$roles,
            'regions'=>$regions,
            'departments'=>$departments
        ]); //pass user and roles data to view

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $user = User::findOrFail($id); //Get role specified by id

        //Validate name, email and password fields
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users,email,'.$id,
            'phone' => 'required|integer|digits:9',
            'address' => 'required|string|max:255',
            'password'=>'required|min:6|confirmed'
        ]);

            $user->name = $request['name'];
            $user->email =  $request['email'];
            $user->phone = $request['phone'];
            $user->region_id = $request['region_id'];
            $user->department_id = $request['department_id'];
            $user->address = $request['address'];
            $user->password =  Hash::make($request['password']);


        $roles = $request['roles']; //Retreive all roles
        $user->save();

        if (isset($roles)) {
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles
        }
        else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        return redirect()->route('users.index')
            ->with('flash_message',
                'User successfully edited.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //Find a user with a given id and delete
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users.index')
            ->with('flash_message',
                'User successfully deleted.');
    }
}
