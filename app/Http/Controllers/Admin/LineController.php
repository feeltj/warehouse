<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Line;
use App\Category;

class LineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lines = Line::all();
        $user = \App\User::count();
        $equiptment = \App\Equiptment::count();
        $category = \App\Category::count();
        $warehouse = \App\Warehouse::count();
        return view('admin.lines.index',[

            'lines'=>$lines,
            'user'=>$user,
            'equiptment'=>$equiptment,
            'category'=>$category,
            'warehouse'=>$warehouse
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.lines.create',[
            'line'=>[],
            'categories'=>Category::orderBy('id','asc')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'value'=>'required|numeric|between:0,99.99',
        ]);
        $line = new Line();
        $line->value = $request['value'];
        $line->category_id = $request['category_id'];
        $line->save();
        return redirect()->route('lines.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $line = Line::findOrFail($id);
        return view('admin.lines.show', [
            'line'=>$line,
            'categories'=>Category::orderBy('id','asc')->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $line = Line::findOrFail($id);
        return view('admin.lines.edit', [
            'line'=>$line,
            'categories'=>Category::orderBy('id','asc')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $line = Line::findOrFail($id);
        $this->validate($request,[
            'value'=>'required|numeric|between:0,99.99',
        ]);
        $line->value = $request['value'];
        $line->category_id = $request['category_id'];
        $line->save();
        return redirect()->route('lines.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $line = Line::findOrFail($id);
        $line->delete();
        return redirect()->route('lines.index');
    }
}
