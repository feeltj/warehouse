<?php

namespace App\Http\Controllers\Admin;

use App\Finalcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subcategory;

class FinalcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = Subcategory::orderBy('id','asc')->get();
        $finalcategories = Finalcategory::all();
        $user = \App\User::count();
        $equiptment = \App\Equiptment::count();
        $category = \App\Category::count();
        $warehouse = \App\Warehouse::count();
        return view('admin.finalcategories.index', [
            'subcategories'=>$subcategories,
            'finalcategories'=>$finalcategories,
            'user'=>$user,
            'equiptment'=>$equiptment,
            'category'=>$category,
            'warehouse'=>$warehouse
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.finalcategories.create',[
            'finalcategory'=>[],
            'subcategories'=>Subcategory::orderBy('id','asc')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=> 'required|max:255',
            'subcategory_id'=>'required|integer',
        ]);
        $finalcategory = new Finalcategory();
        $finalcategory->name = $request['name'];
        $finalcategory->subcategory_id = $request['subcategory_id'];
        $finalcategory->save();
        return redirect()->route('finalcategories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $finalcategory = Finalcategory::findOrFail($id);
        return view('admin.finalcategories.show', [
            'finalcategory'=>$finalcategory,
            'subcategories'=>Subcategory::orderBy('id','asc')->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $finalcategory = Finalcategory::findOrFail($id);
        return view('admin.finalcategories.edit', [
            'finalcategory'=>$finalcategory,
            'subcategories'=>Subcategory::orderBy('id','asc')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $finalcategory = Finalcategory::findOrFail($id);
        $this->validate($request,[
            'name'=> 'required|max:255',
            'subcategory_id'=>'required|integer',
        ]);

        $finalcategory->name = $request['name'];
        $finalcategory->subcategory_id = $request['subcategory_id'];
        $finalcategory->save();
        return redirect()->route('finalcategories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $finalcategory = Finalcategory::findOrFail($id);
        $finalcategory->delete();
        return redirect()->route('finalcategories.index');
    }
}
