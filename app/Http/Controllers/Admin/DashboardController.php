<?php

namespace App\Http\Controllers\Admin;


use App\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Equiptment;
//use App\Equiptstatus;
//use App\Warehouse;
use DB;

class DashboardController extends Controller
{
    public function index(){
        return redirect()->route('admin.dashboard');
    }
    public function dashboard(){

        $users = User::all();
        $equiptments = Equiptment::all();
        $regions = Region::all();
        //$equiptstatuses = Equiptstatus::all();
        //$warehouses = Warehouse::all();
        $stock_amount = DB::table('stocks')->get();

        if($stock_amount->isEmpty()){
            //dd($stock_amount);
            $user = \App\User::count();
            $equiptment = \App\Equiptment::count();
            $category = \App\Category::count();
            $region = \App\Region::count();
            $department = \App\Department::count();
            $tolling = \App\TollingObject::count();
            $line = \App\Line::count();
            $warehouse = \App\Warehouse::count();
            return view('admin.dashboard', [
                'user' => $user,
                'users' => $users,
                'equiptments' => $equiptments,
                'equiptment' => $equiptment,
                'category' => $category,
                'warehouse' => $warehouse,
                'region' => $region,
                'department' => $department,
                'tolling' => $tolling,
                'line' => $line,
                'stock_amount' => $stock_amount,
                'regions'=>$regions
                //'stock_s'=>$stock_s
                //'s'=>$s,
            ]);
        }
        else {
            foreach ($users as $user) {
                foreach ($equiptments as $equiptment) {
                    foreach ($regions as $region) {
                        // $s = DB::table('stocks')
                        //   ->select(DB::raw('count(*) as equiptment_id, user_id'))
                        // ->where('user_id', '<>', $user->id)
                        //->orWhere('equiptment_id', '<>', $equiptment->id)
                        //->groupBy('user_id')
                        //->get();

                        $stock_amount = DB::table('stocks')
                            ->select(DB::raw('COUNT(stocks.equiptment_id) as equiptment_id, user_id, region_id'), 'equiptments.name')
                            ->where('user_id', '<>', $user->id)
                            ->orWhere('equiptment_id', '=', $equiptment->id)
                            ->orWhere('region_id', '<>', $region->id)
                            ->leftjoin('equiptments', 'equiptments.id', '=', 'stocks.equiptment_id')->groupBy('name', 'user_id', 'region_id')->get();
                        //dd($stock_amount);


                        //$stock_s = DB::table('stocks')
                        // ->select(DB::raw('COUNT(stocks.equiptment_id) as equiptment_id, equiptstatus_id'), 'equiptstatuses.name')
                        //->where('user_id', '<>', $user->id)
                        // ->where('equiptstatus_id', '<>', $equiptstatus->id)
                        // ->orWhere('amount', '=', 1)
                        //   ->leftjoin('equiptstatuses', 'equiptstatuses.id', '=', 'stocks.equiptstatus_id')->groupBy('name', 'equiptstatus_id')->get();
                        // dd($stock_s);

                        // dd($u);

                        $user = \App\User::count();
                        $equiptment = \App\Equiptment::count();
                        $category = \App\Category::count();
                        $region = \App\Region::count();
                        $department = \App\Department::count();
                        $tolling = \App\TollingObject::count();
                        $line = \App\Line::count();
                        $warehouse = \App\Warehouse::count();
                        return view('admin.dashboard', [
                            'user' => $user,
                            'users' => $users,
                            'equiptments' => $equiptments,
                            'equiptment' => $equiptment,
                            'category' => $category,
                            'warehouse' => $warehouse,
                            'region' => $region,
                            'department' => $department,
                            'tolling' => $tolling,
                            'line' => $line,
                            'stock_amount' => $stock_amount,
                            'regions' => $regions
                            //'stock_s'=>$stock_s
                            //'s'=>$s,
                        ]);
                    }
                }
            }
        }

    }
    public function contacts(){
        return view('admin.contacts');
    }
    public function profile(){
        return view('admin.profile');
    }
    public function balance(){
        return view('admin.balance');
    }
    public function passages()
    {
        return view('admin.passages');
    }
    public function settings()
    {
        return view('admin.settings');
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush(); // this method should be called after we ensure that there is no logged in guards left

        $request->session()->regenerate(); //same

        return redirect('/admin');
    }
    public function tickets(){
        return view('tickets');
    }

    public function forms(){
        return view('forms');
    }
    public function tables(){
        return view('tables');
    }
    public function buttons(){
        return view('buttons');
    }



}
