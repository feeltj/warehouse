<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Warehouse;
use App\Region;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warehouses = Warehouse::all();
        $regions = Region::all();
        $user = \App\User::count();
        $equiptment = \App\Equiptment::count();
        $category = \App\Category::count();
        $warehouse = \App\Warehouse::count();
        return view('admin.warehouses.index',[
            'warehouses'=>$warehouses,
            'regions'=>$regions,
            'user'=>$user,
            'equiptment'=>$equiptment,
            'category'=>$category,
            'warehouse'=>$warehouse
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = Region::all();
        return view('admin.warehouses.create',[
            'warehouse'=>[],
            'regions'=>$regions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|max:40',
            'region_id'=>'required'
        ]);
        $warehouse = new Warehouse();
        $warehouse->name = $request->name;
        $warehouse->region_id = $request->region_id;
        $warehouse->save();
        return redirect()->route('warehouses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $regions = Region::all();
        $warehouse = Warehouse::findOrFail($id);
        return view('admin.warehouses.show',[
            'warehouse'=>$warehouse,
            'regions'=>$regions
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $regions = Region::all();
        $warehouse = Warehouse::findOrFail($id);
        return view('admin.warehouses.edit',[
            'warehouse'=>$warehouse,
            'regions'=>$regions
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $warehouse = Warehouse::findOrFail($id);
        $this->validate($request,[
            'name'=>'required|max:40'
        ]);
        $warehouse->name = $request->name;
        $warehouse->region_id = $request->region_id;
        $warehouse->save();
        return redirect()->route('warehouses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $warehouse = Warehouse::findOrFail($id);
        $warehouse->delete();
        return redirect()->route('warehouses.index');
    }
}
