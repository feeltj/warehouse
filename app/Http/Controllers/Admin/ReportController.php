<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Equiptment;
use App\Subcategory;
use App\Warehouse;
use App\Equiptstatus;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function index(){
        $users = User::all();
        $subcategories = Subcategory::all();
        $equiptments = Equiptment::all();
        $warehouses = Warehouse::all();
        $equiptstatuses = Equiptstatus::all();
        $stock_amount = DB::table('stocks')->get();
        $stock_w = DB::table('stocks')->get();
        $stock_s = DB::table('stocks')->get();
        if($stock_amount->isEmpty()) {
            return view('admin.reports.index', [
                //'user' => $user,
                'users' => $users,
                'stock_amount' => $stock_amount,
                'subcategories' => $subcategories,
                'equiptments' => $equiptments,
                'stock_w' => $stock_w,
                'stock_s' => $stock_s
                //'s'=>$s,
            ]);
        }
        else {
            foreach ($users as $user) {
                foreach ($equiptments as $equiptment) {
                    foreach ($warehouses as $warehouse) {
                        foreach ($equiptstatuses as $equiptstatus) {
                            $stock_amount = DB::table('stocks')
                                ->select(DB::raw('COUNT(stocks.equiptment_id) as equiptment_id, user_id'), 'equiptments.name')
                                ->where('user_id', '<>', $user->id)
                                ->orWhere('equiptment_id', '=', $equiptment->id)
                                ->leftjoin('equiptments', 'equiptments.id', '=', 'stocks.equiptment_id')->groupBy('name', 'user_id')->get();

                            $stock_w = DB::table('stocks')
                                ->select(DB::raw('COUNT(stocks.equiptment_id) as equiptment_id, warehouse_id'), 'warehouses.name')
                                ->where('warehouse_id', '<>', $warehouse->id)
                                ->orWhere('amount', '=', 1)
                                ->join('warehouses', 'warehouses.id', '=', 'stocks.warehouse_id')->groupBy('name', 'warehouse_id')->get();

                            $stock_s = DB::table('stocks')
                                ->select(DB::raw('COUNT(stocks.equiptment_id) as equiptment_id, equiptstatus_id'), 'equiptstatuses.name')
                                //->where('user_id', '<>', $user->id)
                                ->where('equiptstatus_id', '<>', $equiptstatus->id)
                                ->orWhere('amount', '=', 1)
                                ->leftjoin('equiptstatuses', 'equiptstatuses.id', '=', 'stocks.equiptstatus_id')->groupBy('name', 'equiptstatus_id')->get();


                            //dd($stock_s);
                            return view('admin.reports.index', [
                                'user' => $user,
                                'users' => $users,
                                'stock_amount' => $stock_amount,
                                'subcategories' => $subcategories,
                                'equiptments' => $equiptments,
                                'stock_w' => $stock_w,
                                'stock_s' => $stock_s
                                //'s'=>$s,
                            ]);
                        }
                    }
                }
            }
        }
    }
}
