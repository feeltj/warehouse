<?php

namespace App\Http\Controllers\Admin;

use App\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TollingObject;

class TollingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tollings = TollingObject::all();
        $user = \App\User::count();
        $equiptment = \App\Equiptment::count();
        $category = \App\Category::count();
        $warehouse = \App\Warehouse::count();
        return view('admin.tollings.index',[
            'tollings'=> $tollings,
            'user'=>$user,
            'equiptment'=>$equiptment,
            'category'=>$category,
            'warehouse'=>$warehouse
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = Region::all();
        return view('admin.tollings.create',[
            'tolling'=>[],
            'regions'=>$regions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'name'=>'required|max:40',
            'region_id'=>'required'
        ]);
        $tolling = new TollingObject();
        $tolling->name = $request->name;
        $tolling->region_id = $request['region_id'];
        $tolling->save();
        return redirect()->route('tollings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tolling = TollingObject::findOrFail($id);
        $regions = Region::all();
        return view('admin.tollings.show', [
            'tolling'=>$tolling,
            'regions'=>$regions
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tolling = TollingObject::findOrFail($id);
        $regions = Region::all();
        return view('admin.tollings.edit', [
            'tolling'=>$tolling,
            'regions'=>$regions
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tolling = TollingObject::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:40',
            'region_id'=>'required'
        ]);
        $tolling->name = $request->name;
        $tolling->region_id = $request['region_id'];
        $tolling->save();
        return redirect()->route('tollings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tolling = TollingObject::findOrFail($id);
        $tolling->delete();

        return redirect()->route('tollings.index')
            ->with('flash_message',
                'User successfully deleted.');
    }
}
