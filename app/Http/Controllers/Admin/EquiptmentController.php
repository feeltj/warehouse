<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Equiptment;
use App\Equiptstatus;
use App\Region;
use App\Stock;
use App\Subcategory;
use App\User;
use App\Warehouse;
use App\Line;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;
use App\Finalcategory;


class EquiptmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $s = $request->input('s');

        $equiptments = Equiptment::search($s)
                        ->orderBy('created_at', 'desc')
                       ->paginate(10);
        $subcategories = Subcategory::all();
        $user = User::count();
        $equiptment = \App\Equiptment::count();

        $category = Category::count();
        $warehouse = Warehouse::count();

        return view('admin.equiptments.index',[

            'equiptments'=>$equiptments,
            'user'=>$user,
            'equiptment'=>$equiptment,
            'category'=>$category,
            'warehouse'=>$warehouse,
            'subcategories'=>$subcategories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$lines = Line::all();
        $categories = Category::all();
        $equiptments = Equiptment::all();
        $equiptstatuses = Equiptstatus::all();
        $subcategories = Subcategory::all();

        return view('admin.equiptments.create',[
            'equiptments'=>$equiptments,
            'equiptstatuses'=>$equiptstatuses,
            'categories'=>$categories ,
            'subcategories'=>$subcategories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$count = count($request->input('name'));
        //for($i = 0; $i < $count; ++$i) {

        $equiptment = Equiptment::create(
            [
                'name' => $request->name,
                'description' => $request->description,
                'price'=> $request->price,
                'quantity' => $request->quantity,
                'category_id'=>$request->category_id,
                'subcategory_id'=>$request->subcategory_id,
                'finalcategory_id'=>$request->fincalcategory_id
            ]);
        for ($i = 1; $i <= $request->quantity; $i++)
        {
            $stock = new Stock();
            $stock->equiptment_id = $equiptment->id;
            /*$stock->equiptstatus_id = $request->equiptstatus_id;
            $stock->region_id = $request->region_id;
            $stock->warehouse_id = $request->warehouse_id;
            $stock->line_id = $request->line_id;
            $stock->user_id = $request->user_id;
            $stock->description = $request-gm>description;*/
            $stock->save();
        }
        return redirect()->route('equiptments.index');


        //}
        //$users = User::all();
        /*$this->validate($request,[
            'name' => 'required|unique:equiptments|max:255',
            'description'=>'required',
            'category_id'=>'required',
            'equiptstatus_id'=>'required',
            'user_id'=>'required',
            'region_id'=>'required',
            'warehouse_id'=>'required'
        ]);


        $equiptment = new Equiptment();
        $equiptment->name = $request->name;
        $equiptment->description = $request['description'];
        $equiptment->quantity = $request['quantity'];
        $equiptment->category_id = $request->category_id;
        $equiptment->subcategory_id = $request->subcategory_id;
        $equiptment->equiptstatus_id = $request->equiptstatus_id;
        $equiptment->user_id = $request->user_id;
        $equiptment->region_id = $request->region_id;
        $equiptment->warehouse_id = $request->warehouse_id;
        $equiptment->line_id = $request->line_id;
        $equiptment->finalcategory_id = $request->finalcategory_id;*/
        /*foreach ($users as $user) {
            if ($request->user_id == $user->id) {

                $objDemo = new \stdClass();
                $objDemo->demo_one = $request->name;
                $objDemo->demo_two = $request->description;
                $objDemo->sender = 'IRS company';
                $objDemo->receiver = $user->name;

                Mail::to($user->email)->send(new DemoEmail($objDemo));
            }
        }*/
        //else{
          //  var_dump($equiptment->user_id);
        //}

        //$equiptment->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $equiptment = Equiptment::findOrFail($id);
        $categories = Category::orderBy('id','asc')->get();
        $equiptstatuses = Equiptstatus::orderBy('id','asc')->get();
        $subcategories = Subcategory::orderBy('id','asc')->get();
        $users = User::all();
        $warehouses = Warehouse::orderBy('id','asc')->get();
        $regions = Region::all();
        $lines = Line::all();
        $finalcategories = Finalcategory::all();
        return view('admin.equiptments.show',[
            'equiptment'=>$equiptment,
            'equiptstatuses'=>$equiptstatuses,
            'categories'=>$categories ,
            'subcategories'=>$subcategories,
            'users'=>$users,
            'warehouses'=>$warehouses,
            'regions'=>$regions,
            'lines'=>$lines,
            'finalcategories'=>$finalcategories
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equiptment = Equiptment::findOrFail($id);
        $categories = Category::orderBy('id','asc')->get();
        $subcategories = Subcategory::orderBy('id','asc')->get();
        $finalcategories = Finalcategory::all();
        return view('admin.equiptments.edit',[
            'equiptment'=>$equiptment,
            'categories'=>$categories ,
            'subcategories'=>$subcategories,

            'finalcategories'=>$finalcategories
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $equiptment = Equiptment::findOrFail($id);
        $this->validate($request,[
            'name' => 'required|max:255',
            'description'=>'required',
            'category_id'=>'required',
            //'equiptstatus_id'=>'required',
            //'user_id'=>'required',
            //'region_id'=>'required',
            //'warehouse_id'=>'required'
        ]);
        $equiptment->name = $request->name;
        $equiptment->description = $request['description'];
        $equiptment->quantity = $request['quantity'];
        $equiptment->category_id = $request->category_id;
        $equiptment->subcategory_id = $request->subcategory_id;
        /*$equiptment->equiptstatus_id = $request->equiptstatus_id;
        $equiptment->user_id = $request->user_id;
        $equiptment->region_id = $request->region_id;
        $equiptment->warehouse_id = $request->warehouse_id;
        $equiptment->line_id = $request->line_id;*/
        $equiptment->finalcategory_id = $request->finalcategory_id;
        $equiptment->save();
        return redirect()->route('equiptments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $equiptment = Equiptment::find($id);
        $equiptment->delete();
        return redirect()->route('equiptments.index');

    }
}
