<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Equiptment;
use App\Equiptstatus;
use App\Finalcategory;
use App\Line;
use App\Region;
use App\Repair;
use App\Stock;
use App\Subcategory;
use App\User;
use App\Warehouse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;
use DB;
class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *a
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $stocks = Stock::orderBy('id','desc')->paginate(10);
        $users = User::all();
        $equiptstatuses = Equiptstatus::all();
        $equiptments = Equiptment::all();
        //$stock = DB::table("stocks")->pluck("equiptment_id");
        //$equiptments = DB::table("equiptments")->whereIn('id', $stock)->pluck("name","id");
        return view('admin.stocks.index',[
           //'stock'=>$stock,
            'equiptments'=>$equiptments,
            'stocks'=>$stocks,
            'users'=>$users,
            'equiptstatuses'=>$equiptstatuses
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $equiptments = Equiptment::all();
        $users = User::all();
        $equiptstatuses = Equiptstatus::all();
        $regions = Region::all();

        return view('admin.stocks.create',[
           'equiptments' =>$equiptments,
            'users'=>$users,
            'equiptstatuses'=>$equiptstatuses,
            'regions'=>$regions
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$stock = new Stock();
        $stock->equiptment_id = $request['equiptment_id'];
        $stock->user_id = $request['user_id'];
        $stock->equiptstatus_id = $request['equiptstatus_id'];
        $stock->region_id = $request['region_id'];
        $stock->price = $request['price'];

            $stock->quantity = $request['quantity'];

            $stock->save();

        return redirect('/admin/stocks');*/
        $stock = Stock::where([
            ['equiptment_id', '=', $request->equiptment_id],
            ['user_id', '=', $request->user_id],
            ['equiptstatus_id', '=', $request->equiptstatus_id],
        ])->first();

        if ($stock) {
            $stock->increment('quantity', $request->quantity);
        } else  {
            $stock = new Stock();
            $stock->equiptment_id = $request['equiptment_id'];
            $stock->user_id = $request['user_id'];
            $stock->equiptstatus_id = $request['equiptstatus_id'];
            $stock->region_id = $request['region_id'];
            //$stock->price = $request['price'];

            $stock->quantity = $request['quantity'];

            $stock->save();

        }
        return redirect('/admin/stocks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $repairs = Repair::all();
        $equiptments = Equiptment::all();
        $stock = Stock::findOrFail($id);
        $equiptstatuses = Equiptstatus::orderBy('id','asc')->get();
        $users = User::all();
        $regions = Region::all();
        $warehouses = Warehouse::all();
        $categories = Category::all();
        $subcategories = Subcategory::all();
        $lines = Line::all();
        $finalcategories = Finalcategory::all();

        return view('admin.stocks.show',[
            'equiptments'=>$equiptments,
             'stock'=>$stock,
            'equiptstatuses'=>$equiptstatuses,
            'users'=>$users,
            'regions'=>$regions,
            'warehouses'=>$warehouses,
            'categories'=>$categories,
            'subcategories'=>$subcategories,
            'lines'=>$lines,
            'finalcategories'=>$finalcategories,
            'repairs'=>$repairs
        ]);
    }
    public function add(Request $request, $id){
        $stock = Stock::findOrFail($id);

        //$stock->description = $request['description'];
        $stock->equiptstatus_id = $request->equiptstatus_id;
        $stock->user_id = $request->user_id;
        $stock->region_id = $request->region_id;
        $stock->warehouse_id = $request->warehouse_id;
        $stock->line_id = $request->line_id;
         $stock->save();

        $repair = new Repair();
        if(!empty($request['value_after']) && !empty($request['value_before']) && !empty($request['description'])) {
            $repair->value_after = $request['value_after'];
            $repair->value_before = $request['value_before'];
            $repair->description = $request['description'];


            $repair->stock_id = $stock->id;
            $repair->save();
            return redirect()->back();
        }
        $request->session()->flash('alert-success', 'Оборудование успешно присвоено!!!');
        //$stock = Stock::findOrFail($id);
        //$stock->value_after = $request['value_after'];
        //dd($stock);
        //$stock->save();
        return redirect()->route('stocks.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equiptments = Equiptment::all();
        $stock = Stock::findOrFail($id);
        $equiptstatuses = Equiptstatus::orderBy('id','asc')->get();
        $users = User::all();
        $regions = Region::all();
        $warehouses = Warehouse::all();
        $categories = Category::all();
        $subcategories = Subcategory::all();
        $lines = Line::all();
        $finalcategories = Finalcategory::all();
        //foreach ($lines as $line){
          //  dd($line->category_id);
        //}

        return view('admin.stocks.edit',[
            'equiptments'=>$equiptments,
            'stock'=>$stock,
            'equiptstatuses'=>$equiptstatuses,
            'users'=>$users,
            'regions'=>$regions,
            'warehouses'=>$warehouses,
            'categories'=>$categories,
            'subcategories'=>$subcategories,
            'lines'=>$lines,
            'finalcategories'=>$finalcategories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stock = Stock::findOrFail($id);
        $stock->description = $request['description'];
        $stock->equiptstatus_id = $request->equiptstatus_id;
        $stock->user_id = $request->user_id;
        $stock->region_id = $request->region_id;
        $stock->warehouse_id = $request->warehouse_id;
        $stock->line_id = $request->line_id;
        $users = User::all();
        $equiptments = Equiptment::all();
        $objDemo = new \stdClass();


        foreach ($users as $user) {


           // foreach ($equiptments as $equiptment) {

                if ($request->user_id == $user->id) {

                    $s = DB::table('stocks')
                        ->select(DB::raw('count(*) as equiptment_id, user_id'))
                        ->where('user_id', '<>', $user->id)
                        ->orWhere('equiptment_id', '<>', $request->equiptment_id)
                        ->groupBy('user_id')
                        ->get();

                    foreach($s as $z) {

                        if( $z->user_id == $user->id) {

                            $objDemo->demo_one = $user->name;
                            foreach($equiptments as $equiptment) {

                                if ($stock->equiptment_id == $equiptment->id) {

                                    $objDemo->demo_two = $equiptment->name;
                                    //dd($objDemo->demo_two);
                                }
                            }
                            $objDemo->demo_three = $z->equiptment_id;
                            $objDemo->sender = 'IRS company';
                            $objDemo->receiver = $user->name;
                            Mail::to($user->email)->send(new DemoEmail($objDemo));


                        }

                    }

                }
                }

           // }

            //Mail::to($user->email)->send(new DemoEmail($objDemo));

        //else{
        //  var_dump($equiptment->user_id);
        //}
        $request->session()->flash('alert-primary', 'Оборудование успешно изменено!!!');
        $stock->save();
        return redirect()->route('stocks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock = Stock::find($id);
        $stock->delete();
        session()->flash('alert-danger', 'Оборудование успешно изменено!!!');
        return redirect()->route('stocks.index');
    }
}
