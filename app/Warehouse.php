<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $fillable = ['id', 'name'];
    public function equiptments(){
        return $this->hasMany('App\Equiptment');
    }
    public function regions(){
        return $this->belongsTo('App\Region');
    }
}
