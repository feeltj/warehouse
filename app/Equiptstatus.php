<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equiptstatus extends Model
{
    protected $fillable = ['id','name'];
    public function equiptments(){
        return $this->hasMany('App\Equiptment');
    }
}
