<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Line extends Model
{
    protected $fillable = ['id','name','value','category_id'];
    public function equiptments()
    {
        return $this->hasMany('App\Equiptment');
    }
    public function categories(){
        return $this->belongsTo('App\Category');
    }
}
