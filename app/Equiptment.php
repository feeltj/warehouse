<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equiptment extends Model
{
    protected $fillable = ['id','name','description','price','quantity','category_id','subcategory_id','finalcategory_id'];
    //Relationship with tables, eloquent models one category has many equipment for example
    public function categories(){
        return $this->belongsTo('App\Category');
    }
    public function subcategories(){
        return $this->belongsTo('App\Subcategory');
    }
    public function equiptstatuses(){
        return $this->belongsTo('App\Equiptstatus');
    }
    public function users(){
        return $this->belongsTo('App\User');
    }
    public function warehouses(){
        return $this->belongsTo('App\Warehouse');
    }
    public function regions()
    {
        return $this->belongsTo('App\Region');
    }
    public function lines()
    {
        return $this->belongsTo('App\Line');
    }
    public function stocks()
    {
        return $this->belongsTo('App\Stock');
    }
    public function scopeSearch($query, $s)
    {
        return $query->where('name','like', '%' .$s. '%');
    }

}
