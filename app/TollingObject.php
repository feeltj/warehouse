<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TollingObject extends Model
{
    protected $fillable = ['id', 'name'];
    public function region()
    {
        return $this->belongsTo('App\Region');
    }
}
