<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Finalcategory extends Model
{
    protected $fillable = ['id', 'name', 'subcategory_id'];
    public function subcategories()
    {
        return $this->belongsTo('App\Subcategory');
    }
}
