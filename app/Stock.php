<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    //
    protected $fillable = ['id', 'quantity','price','equiptstatus_id'];
    public function regions()
    {
        return $this->belongsTo('App\Region');
    }
    public function equiptments()
    {
        return $this->hasMany('App\Equiptment');
    }


}
