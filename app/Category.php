<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['id', 'name'];

    public function subcategories(){
        return $this->hasMany('App\Subcategory');
    }
    public function equiptments(){
        return $this->hasMany('App\Equiptment');
    }
    public function lines()
    {
        return $this->hasMany('App\Line');
    }
}
