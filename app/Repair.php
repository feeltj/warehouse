<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repair extends Model
{
    protected $fillable = ['description','value_before','value_after','stock_id', 'created_at','updated_at'];
}
