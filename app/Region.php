<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable = ['id', 'name'];
    public function equiptments(){
        return $this->hasMany('App\Equiptment');
    }
    public function users(){
        return $this->hasMany('App\User');
    }
    public function warehouses()
    {
        return $this->hasMany('App\Warehouses');
    }
    public function tolling_objects()
    {
        return $this->hasMany('App\TollingObject');
    }
    public function stocks()
    {
        return $this->hasMany('App\Stock');
    }
}
