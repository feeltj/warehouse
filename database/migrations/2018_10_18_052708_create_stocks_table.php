<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equiptment_id')->index()->unsigned()->default(0);
            $table->foreign('equiptment_id')->references('id')->on('equiptments')->onDelete('cascade');
            $table->integer('user_id')->index()->unsigned()->default(0);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('equiptstatus_id')->unsigned()->default(0);
            $table->foreign('equiptstatus_id')->references('id')->on('equiptstatuses')->onDelete('cascade');
            $table->integer('region_id')->unsigned()->default(0);
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
            $table->string('quantity');
            $table->string('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
