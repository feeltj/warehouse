<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinalcategoryIdToEquiptmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equiptments', function (Blueprint $table) {
            $table->integer('finalcategory_id')->unsigned()->nullable()->after('line_id');
            $table->foreign('finalcategory_id')->references('id')->on('finalcategories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equiptments', function (Blueprint $table) {
            //
        });
    }
}
