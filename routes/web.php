<?php
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
   // return view('welcome');
//});

Route::get('/', '\App\Http\Controllers\Auth\LoginController@showLoginForm');
Auth::routes();


Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware' => ['auth']], function(){
    Route::group(['middleware'=>['role:Администратор|Заведующий Складом']],function(){

    Route::get('/reports',[
        'as'=>'reports.index',
        'uses'=>'ReportController@index'
    ]);
        Route::get('logout/', 'DashboardController@logout')->name('admin.logout');
    Route::get('/', 'DashboardController@dashboard')->name('admin.index');
        //Route::get('profile', 'DashboardController@profile')->name('profile');
        Route::post('/stocks/{id}',[
            'as' => 'stocks.add',
            'uses' => 'StockController@add'
        ]);


        Route::resource('stocks','StockController');
        Route::resource('finalcategories','FinalcategoryController');
        Route::resource('lines','LineController');
        Route::resource('equiptments', 'EquiptmentController');
        Route::resource('equiptstatuses', 'EquiptstatusController');
        Route::resource('subcategories','SubcategoryController');
        Route::resource('categories','CategoryController');
        Route::resource('regions','RegionController');
        Route::resource('departments','DepartmentController');
        Route::resource('tollings','TollingController');
        Route::resource('roles','RoleController');
        Route::resource('warehouses','WarehouseController');
        Route::resource('users','UserController');
        Route::resource('permissions', 'PermissionController');



        Route::get('/information/create/ajax-state',function()
        {
            $cat_id = Input::get('category_id');
            $subcategories = \App\Subcategory::where('category_id','=',$cat_id)->get();
            return Response::json($subcategories);

        });

        Route::get('/information/create/ajax',function()
        {
            $region_id = Input::get('region_id');
            $warehouses = \App\Warehouse::where('region_id','=',$region_id)->get();
            return Response::json($warehouses);

        });
        Route::get('/information/create',function()
        {
            $cat_id = Input::get('category_id');
            $lines = \App\Line::where('category_id','=',$cat_id)->get();
            return Response::json($lines);

        });

        Route::get('/information/final',function()
        {
            $subcat_id = Input::get('subcategory_id');
            $finalcategories = \App\Finalcategory::where('subcategory_id','=',$subcat_id)->get();
            return Response::json($finalcategories);

        });
    });



});

Route::group(['middleware'=>['role:Специалисты']],function(){
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contacts', 'HomeController@contacts')->name('contacts');
Route::get('/tickets', 'HomeController@tickets')->name('tickets');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::get('/forms', 'HomeController@forms')->name('forms');
Route::get('/tables', 'HomeController@tables')->name('tables');
Route::get('/buttons', 'HomeController@buttons')->name('buttons');
Route::get('/balance', 'HomeController@balance')->name('balance');
Route::get('/passages', 'HomeController@passages')->name('passages');
Route::get('/settings', 'HomeController@settings')->name('settings');
});


