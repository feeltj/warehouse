import Vue from 'vue';
window.Vue = Vue;


const app = new Vue({
    el: '#app',
    components: { ExampleComponent },
    router
})