@extends('layouts.app')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Мой профиль
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i>Домой</a></li>
                <li class="breadcrumb-item active">Мой профиль</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="fa fa-exchange"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">10<small>%</small></span>
                            <span class="info-box-text">Тарифы</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-primary"><i class="fa fa-balance-scale"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">760</span>
                            <span class="info-box-text">Средний Баланс</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fa fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">2,000</span>
                            <span class="info-box-text">Пользователи</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fa fa-car"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">04-07-2018</span>
                            <span class="info-box-text">Проезд Последний</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
            <section class="content">

                <div class="row">
                    <div class="col-xl-4 col-lg-5">

                        <!-- Profile Image -->
                        <div class="box">
                            <div class="box-body box-profile">
                                <img class="profile-user-img rounded-circle img-fluid mx-auto d-block" src="images/user5-128x128.jpg" alt="User profile picture">

                                <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

                                <p class="text-muted text-center">Партнер</p>


                                <div class="row">
                                    <div class="col-12">
                                        <div class="profile-user-info">
                                            <p>Email</p>
                                            <h6 class="margin-bottom">{{ Auth::user()->email }}</h6>
                                            <p>Телефон</p>
                                            <h6 class="margin-bottom">+992 908 99 7474</h6>
                                            <p>Адрес</p>
                                            <h6 class="margin-bottom">Айни 26 квартира 7</h6>
                                            <p class="margin-bottom">Социальные сети</p>
                                            <div class="user-social-acount">
                                                <button class="btn btn-circle btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></button>
                                                <button class="btn btn-circle btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></button>
                                                <button class="btn btn-circle btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-xl-8 col-lg-7">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">

                                <li><a class="active" href="#timeline" data-toggle="tab">Главная</a></li>
                                <li><a href="#settings" data-toggle="tab">Настройки</a></li>
                            </ul>

                            <div class="tab-content">

                                <div class="active tab-pane" id="timeline">
                                    <!-- The timeline -->
                                    <ul class="timeline">
                                        <!-- timeline time label -->
                                        <li class="time-label">
						  <span class="bg-info">
							15 Jan. 2017
						  </span>
                                        </li>
                                        <!-- /.timeline-label -->
                                        <!-- timeline item -->
                                        <li>
                                            <i class="fa fa-car bg-blue"></i>

                                            <div class="timeline-item">
                                                <span class="time"><i class="fa fa-clock-o"></i> 11:48</span>

                                                <h3 class="timeline-header"><a href="#">Проезд</a> последний</h3>

                                                <div class="timeline-body">
                                                    Проезд последний был зафиксирован в Истаравшане . ...
                                                </div>
                                                <div class="timeline-footer text-right">
                                                    <a href="#" class="btn btn-primary btn-sm">Подробнее</a>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- END timeline item -->
                                        <!-- timeline item -->
                                        <li>
                                            <i class="fa fa-balance-scale bg-blue"></i>

                                            <div class="timeline-item">
                                                <span class="time"><i class="fa fa-clock-o"></i> 11 минут назад</span>

                                                <h3 class="timeline-header no-border"><a href="#">Текущий баланс </a> 200 сомони</h3>
                                            </div>
                                        </li>
                                        <!-- END timeline item -->
                                        <!-- timeline item -->
                                        <li>
                                            <i class="fa fa-exchange bg-blue"></i>

                                            <div class="timeline-item">
                                                <span class="time"><i class="fa fa-clock-o"></i> 55 минут назад</span>

                                                <h3 class="timeline-header"><a href="#">Тарифы </a> с 10% скидкой</h3>
                                                <div class="timeline-footer text-right">
                                                    <a class="btn bg-purple btn-flat btn-sm">Подробнее</a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.tab-pane -->

                                <div class="tab-pane" id="settings">
                                    <form class="form-horizontal form-element col-12">
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 control-label">Пароль</label>

                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="inputPassword" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPassword1" type="password" class="col-sm-2 control-label">Потвердить пароль</label>

                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="inputPassword1" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="ml-auto col-sm-10">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="basic_checkbox_1" checked="">
                                                    <label for="basic_checkbox_1"> Я согласен с </label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<a href="#">условиями соглашения</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="ml-auto col-sm-10">
                                                <button type="submit" class="btn btn-success">Изменить</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </section>

            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right d-none d-sm-inline-block">
            <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)">FAQ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://themeforest.net/item/minimal-pro-responsive-bootstrap-4-admin-dashboard-template-webapp-template/21067203">Purchase Now</a>
                </li>
            </ul>
        </div>
        &copy; 2017 <a href="https://www.multipurposethemes.com/">Multi-Purpose Themes</a>. All Rights Reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-light">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-cog fa-spin"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Admin Birthday</h4>

                                <p>Will be July 24th</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-user bg-yellow"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Jhone Updated His Profile</h4>

                                <p>New Email : jhone_doe@demo.com</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Disha Joined Mailing List</h4>

                                <p>disha@demo.com</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-file-code-o bg-green"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Code Change</h4>

                                <p>Execution time 15 Days</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Web Design
                                <span class="label label-danger pull-right">40%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 40%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Update Data
                                <span class="label label-success pull-right">75%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-success" style="width: 75%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Order Process
                                <span class="label label-warning pull-right">89%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-warning" style="width: 89%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Development
                                <span class="label label-primary pull-right">72%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-primary" style="width: 72%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <input type="checkbox" id="report_panel" class="chk-col-grey" >
                        <label for="report_panel" class="control-sidebar-subheading ">Report panel usage</label>

                        <p>
                            general settings information
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <input type="checkbox" id="allow_mail" class="chk-col-grey" >
                        <label for="allow_mail" class="control-sidebar-subheading ">Mail redirect</label>

                        <p>
                            Other sets of options are available
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <input type="checkbox" id="expose_author" class="chk-col-grey" >
                        <label for="expose_author" class="control-sidebar-subheading ">Expose author name</label>

                        <p>
                            Allow the user to show his name in blog posts
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <h3 class="control-sidebar-heading">Chat Settings</h3>

                    <div class="form-group">
                        <input type="checkbox" id="show_me_online" class="chk-col-grey" >
                        <label for="show_me_online" class="control-sidebar-subheading ">Show me as online</label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <input type="checkbox" id="off_notifications" class="chk-col-grey" >
                        <label for="off_notifications" class="control-sidebar-subheading ">Turn off notifications</label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            <a href="javascript:void(0)" class="text-red margin-r-5"><i class="fa fa-trash-o"></i></a>
                            Delete chat history
                        </label>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

@endsection