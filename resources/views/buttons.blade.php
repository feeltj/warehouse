@extends('layouts.app')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Buttons UI
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="#">UI</a></li>
            <li class="breadcrumb-item active">Buttons</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <i class="fa fa-edit"></i>

                        <h3 class="box-title">Buttons</h3>
                    </div>
                    <div class="box-body res-tb-block">
                        <p>Various types of buttons. Using the base class <code>.btn</code></p>
                        <table class="table table-bordered text-center table-responsive">
                            <tr>
                                <th>Normal</th>
                                <th>Large <code>.btn-lg</code></th>
                                <th>Small <code>.btn-sm</code></th>
                                <th>X-Small <code>.btn-xs</code></th>
                                <th>Flat <code>.btn-flat</code></th>
                                <th>Disabled <code>.disabled</code></th>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-default">Default</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-default btn-lg">Default</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-default btn-sm">Default</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-default btn-xs">Default</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-default btn-flat">Default</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-default disabled">Default</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary">Primary</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary btn-lg">Primary</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary btn-sm">Primary</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary btn-xs">Primary</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary btn-flat">Primary</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary disabled">Primary</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-success">Success</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-success btn-lg">Success</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-success btn-sm">Success</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-success btn-xs">Success</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-success btn-flat">Success</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-success disabled">Success</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-info">Info</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-info btn-lg">Info</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-info btn-sm">Info</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-info btn-xs">Info</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-info btn-flat">Info</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-info disabled">Info</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger">Danger</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger btn-lg">Danger</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger btn-sm">Danger</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger btn-xs">Danger</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger btn-flat">Danger</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger disabled">Danger</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning">Warning</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning btn-lg">Warning</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning btn-sm">Warning</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning btn-xs">Warning</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning btn-flat">Warning</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning disabled">Warning</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- ./row -->
        <div class="row">
            <div class="col-12 col-xl-6">

                <!-- Horizontal grouping -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Horizontal Button Group</h3>
                    </div>
                    <div class="box-body res-tb-block">
                        <p>
                            Horizontal button groups are easy to create with bootstrap. Just add your buttons
                            inside <code>&lt;div class="btn-group"&gt;&lt;/div&gt;</code>
                        </p>

                        <table class="table table-bordered table-responsive">
                            <tr>
                                <th>Button</th>
                                <th>Icons</th>
                                <th>Dropdown</th>
                            </tr>

                            <!-- Info -->
                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info">Left</button>
                                        <button type="button" class="btn btn-info">Middle</button>
                                        <button type="button" class="btn btn-info">Right</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-success"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-success"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning">1</button>
                                        <button type="button" class="btn btn-warning">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <a class="dropdown-item" href="#">Another action</a>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- /. info -->
                        </table>
                    </div>
                </div>
                <!-- /.box -->

                <!-- Vertical grouping -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Vertical Button Group</h3>
                    </div>
                    <div class="box-body table-responsive">

                        <p>
                            Vertical button groups are easy to create with bootstrap. Just add your buttons
                            inside <code>&lt;div class="btn-group-vertical"&gt;&lt;/div&gt;</code>
                        </p>

                        <table class="table table-bordered">
                            <tr>
                                <th>Button</th>
                                <th>Icons</th>
                                <th>Dropdown</th>
                            </tr>
                            <!-- Info -->
                            <tr>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-info">Top</button>
                                        <button type="button" class="btn btn-info">Middle</button>
                                        <button type="button" class="btn btn-info">Bottom</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-warning"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-warning"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-warning"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-danger">1</button>
                                        <button type="button" class="btn btn-danger">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-caret-down"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <a class="dropdown-item" href="#">Another action</a>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- /. info -->
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- social buttons -->
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Social buttons Icon with Name</h4>
                        <h6 class="subtitle mb-0">Use a classes <code>btn btn-social btn-bitbucket /  &lt; class="fa fa-bitbucket"&gt;&lt;/i&gt;</code> to quickly create a General btn.</h6>
                    </div>
                    <div class="box-body demo-social">
                        <button class="btn btn-block btn-social btn-bitbucket">
                            <i class="fa fa-bitbucket"></i> Sign in with Bitbucket
                        </button>
                        <button class="btn btn-block btn-social btn-dropbox">
                            <i class="fa fa-dropbox"></i> Sign in with Dropbox
                        </button>
                        <button class="btn btn-block btn-social btn-facebook">
                            <i class="fa fa-facebook"></i> Sign in with Facebook
                        </button>
                        <button class="btn btn-block btn-social btn-flickr">
                            <i class="fa fa-flickr"></i> Sign in with Flickr
                        </button>
                        <button class="btn btn-block btn-social btn-foursquare">
                            <i class="fa fa-foursquare"></i> Sign in with Foursquare
                        </button>
                        <button class="btn btn-block btn-social btn-github">
                            <i class="fa fa-github"></i> Sign in with GitHub
                        </button>
                        <button class="btn btn-block btn-social btn-google">
                            <i class="fa fa-google-plus"></i> Sign in with Google
                        </button>
                        <button class="btn btn-block btn-social btn-instagram">
                            <i class="fa fa-instagram"></i> Sign in with Instagram
                        </button>
                        <button class="btn btn-block btn-social btn-linkedin">
                            <i class="fa fa-linkedin"></i> Sign in with LinkedIn
                        </button>
                        <button class="btn btn-block btn-social btn-tumblr">
                            <i class="fa fa-tumblr"></i> Sign in with Tumblr
                        </button>
                        <button class="btn btn-block btn-social btn-twitter">
                            <i class="fa fa-twitter"></i> Sign in with Twitter
                        </button>
                        <button class="btn btn-block btn-social btn-vk">
                            <i class="fa fa-vk"></i> Sign in with VK
                        </button>
                        <br>

                        <div class="text-left margin-top-20">
                            <h4 class="box-title">Social buttons Icon</h4>
                            <p>Use a classes <code>btn btn-social-icon btn-bitbucket</code> to quickly create a General btn.</p>
                            <button class="btn btn-social-icon btn-bitbucket"><i class="fa fa-bitbucket"></i></button>
                            <button class="btn btn-social-icon btn-dropbox"><i class="fa fa-dropbox"></i></button>
                            <button class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></button>
                            <button class="btn btn-social-icon btn-flickr"><i class="fa fa-flickr"></i></button>
                            <button class="btn btn-social-icon btn-foursquare"><i class="fa fa-foursquare"></i></button>
                            <button class="btn btn-social-icon btn-github"><i class="fa fa-github"></i></button>
                            <button class="btn btn-social-icon btn-google"><i class="fa fa-google-plus"></i></button>
                            <button class="btn btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></button>
                            <button class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></button>
                            <button class="btn btn-social-icon btn-tumblr"><i class="fa fa-tumblr"></i></button>
                            <button class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></button>
                            <button class="btn btn-social-icon btn-vk"><i class="fa fa-vk"></i></button>
                        </div>

                        <div class="text-left margin-top-20">
                            <h4 class="box-title">With Circle buttons</h4>
                            <p>Use a classes <code>btn btn-social-icon btn-circle</code> to quickly create a General btn.</p>
                            <button class="btn btn-social-icon btn-circle btn-bitbucket"><i class="fa fa-bitbucket"></i></button>
                            <button class="btn btn-social-icon btn-circle btn-dropbox"><i class="fa fa-dropbox"></i></button>
                            <button class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></button>
                            <button class="btn btn-social-icon btn-circle btn-flickr"><i class="fa fa-flickr"></i></button>
                            <button class="btn btn-social-icon btn-circle btn-foursquare"><i class="fa fa-foursquare"></i></button>
                            <button class="btn btn-social-icon btn-circle btn-github"><i class="fa fa-github"></i></button>
                            <button class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></button>
                            <button class="btn btn-social-icon btn-circle btn-instagram"><i class="fa fa-instagram"></i></button>
                            <button class="btn btn-social-icon btn-circle btn-linkedin"><i class="fa fa-linkedin"></i></button>
                            <button class="btn btn-social-icon btn-circle btn-tumblr"><i class="fa fa-tumblr"></i></button>
                            <button class="btn btn-social-icon btn-circle btn-twitter"><i class="fa fa-twitter"></i></button>
                            <button class="btn btn-social-icon btn-circle btn-vk"><i class="fa fa-vk"></i></button>
                        </div>

                        <div class="text-left margin-top-20">
                            <h4 class="box-title">With rounded buttons</h4>
                            <p>Use a classes <code>btn btn-social-icon btn-rounded</code> to quickly create a General btn.</p>
                            <button class="btn btn-social-icon btn-rounded btn-bitbucket"><i class="fa fa-bitbucket"></i></button>
                            <button class="btn btn-social-icon btn-rounded btn-dropbox"><i class="fa fa-dropbox"></i></button>
                            <button class="btn btn-social-icon btn-rounded btn-facebook"><i class="fa fa-facebook"></i></button>
                            <button class="btn btn-social-icon btn-rounded btn-flickr"><i class="fa fa-flickr"></i></button>
                            <button class="btn btn-social-icon btn-rounded btn-foursquare"><i class="fa fa-foursquare"></i></button>
                            <button class="btn btn-social-icon btn-rounded btn-github"><i class="fa fa-github"></i></button>
                            <button class="btn btn-social-icon btn-rounded btn-google"><i class="fa fa-google-plus"></i></button>
                            <button class="btn btn-social-icon btn-rounded btn-instagram"><i class="fa fa-instagram"></i></button>
                            <button class="btn btn-social-icon btn-rounded btn-linkedin"><i class="fa fa-linkedin"></i></button>
                            <button class="btn btn-social-icon btn-rounded btn-tumblr"><i class="fa fa-tumblr"></i></button>
                            <button class="btn btn-social-icon btn-rounded btn-twitter"><i class="fa fa-twitter"></i></button>
                            <button class="btn btn-social-icon btn-rounded btn-vk"><i class="fa fa-vk"></i></button>
                        </div>

                    </div>
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
            <div class="col-12 col-xl-6">

                <!-- Various colors -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Different colors</h3>
                    </div>
                    <div class="box-body">
                        <p>Mix and match with various background colors. Use base class <code>.btn</code> and add any available
                            <code>.bg-*</code> class</p>
                        <!-- You may notice a .margin class added here but that's only to make the content display correctly without having to use a table-->
                        <p>
                            <button type="button" class="btn bg-maroon btn-flat margin">.btn.bg-maroon.btn-flat</button>
                            <button type="button" class="btn bg-purple btn-flat margin">.btn.bg-purple.btn-flat</button>
                            <button type="button" class="btn bg-navy btn-flat margin">.btn.bg-navy.btn-flat</button>
                            <button type="button" class="btn bg-orange btn-flat margin">.btn.bg-orange.btn-flat</button>
                            <button type="button" class="btn bg-olive btn-flat margin">.btn.bg-olive.btn-flat</button>
                        </p>

                        <p>
                            <button type="button" class="btn bg-maroon margin">.btn.bg-maroon</button>
                            <button type="button" class="btn bg-purple margin">.btn.bg-purple</button>
                            <button type="button" class="btn bg-navy margin">.btn.bg-navy</button>
                            <button type="button" class="btn bg-orange margin">.btn.bg-orange</button>
                            <button type="button" class="btn bg-olive margin">.btn.bg-olive</button>
                        </p>
                    </div>
                </div>
                <!-- /.box -->

                <!-- Application buttons -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Application Buttons</h3>
                    </div>
                    <div class="box-body">
                        <p>Add the classes <code>.btn.btn-app</code> to an <code>&lt;a&gt;</code> tag to achieve the following:</p>
                        <a class="btn btn-app bg-blue">
                            <i class="fa fa-edit"></i> Edit
                        </a>
                        <a class="btn btn-app bg-green">
                            <i class="fa fa-play"></i> Play
                        </a>
                        <a class="btn btn-app bg-purple">
                            <i class="fa fa-pause"></i> Pause
                        </a>
                        <a class="btn btn-app bg-teal">
                            <i class="fa fa-stop"></i> Stop
                        </a>
                        <a class="btn btn-app bg-yellow">
                            <i class="fa fa-repeat"></i> Repeat
                        </a>
                        <a class="btn btn-app bg-olive">
                            <i class="fa fa-save"></i> Save
                        </a>
                        <a class="btn btn-app bg-red">
                            <span class="badge bg-yellow">12</span>
                            <i class="fa fa-bullhorn"></i> Notifications
                        </a>
                        <a class="btn btn-app bg-olive">
                            <span class="badge bg-purple">41</span>
                            <i class="fa fa-users"></i> Users
                        </a>
                        <a class="btn btn-app bg-purple">
                            <span class="badge bg-teal">45</span>
                            <i class="fa fa-inbox"></i> Orders
                        </a>
                        <a class="btn btn-app bg-teal">
                            <span class="badge bg-aqua">2</span>
                            <i class="fa fa-envelope"></i> Inbox
                        </a>
                        <a class="btn btn-app bg-red">
                            <span class="badge bg-green">48</span>
                            <i class="fa fa-barcode"></i> Products
                        </a>
                        <a class="btn btn-app bg-blue">
                            <span class="badge bg-red">31</span>
                            <i class="fa fa-heart-o"></i> Likes
                        </a>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- split buttons box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Split buttons</h3>
                    </div>
                    <div class="box-body">
                        <!-- Split button -->
                        <p>Normal split buttons:</p>

                        <div class="margin">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default">Action</button>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info">Action</button>
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-danger">Action</button>
                                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-success">Action</button>
                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-warning">Action</button>
                                <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                        </div>
                        <!-- flat split buttons -->
                        <p>Flat split buttons:</p>

                        <div class="margin">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-flat">Action</button>
                                <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info btn-flat">Action</button>
                                <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-danger btn-flat">Action</button>
                                <button type="button" class="btn btn-danger btn-flat dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-success btn-flat">Action</button>
                                <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-warning btn-flat">Action</button>
                                <button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- end split buttons box -->

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Button Addon</h3>
                    </div>
                    <div class="box-body">
                        <p>With dropdown</p>

                        <div class="input-group margin">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action</button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                            <!-- /btn-group -->
                            <input type="text" class="form-control">
                        </div>
                        <!-- /input-group -->
                        <p>Normal</p>

                        <div class="input-group margin">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-danger">Action</button>
                            </div>
                            <!-- /btn-group -->
                            <input type="text" class="form-control">
                        </div>
                        <!-- /input-group -->
                        <p>Flat</p>

                        <div class="input-group margin">
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat">Go!</button>
                    </span>
                        </div>
                        <!-- /input-group -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


            </div>
            <!-- /.col -->
        </div>
        <!-- /. row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)">FAQ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.multipurposethemes.com/product/minimal-pro-responsive-bootstrap-4-admin-dashboard-template-webapp-template/">Purchase Now</a>
            </li>
        </ul>
    </div>
    &copy; 2017 <a href="https://www.multipurposethemes.com/">Multi-Purpose Themes</a>. All Rights Reserved.
</footer>
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-light">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-cog fa-spin"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Admin Birthday</h4>

                            <p>Will be July 24th</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-user bg-yellow"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Jhone Updated His Profile</h4>

                            <p>New Email : jhone_doe@demo.com</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Disha Joined Mailing List</h4>

                            <p>disha@demo.com</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-file-code-o bg-green"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Code Change</h4>

                            <p>Execution time 15 Days</p>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Web Design
                            <span class="label label-danger pull-right">40%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-danger" style="width: 40%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Update Data
                            <span class="label label-success pull-right">75%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-success" style="width: 75%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Order Process
                            <span class="label label-warning pull-right">89%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-warning" style="width: 89%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Development
                            <span class="label label-primary pull-right">72%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-primary" style="width: 72%"></div>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
                <h3 class="control-sidebar-heading">General Settings</h3>

                <div class="form-group">
                    <input type="checkbox" id="report_panel" class="chk-col-grey" >
                    <label for="report_panel" class="control-sidebar-subheading ">Report panel usage</label>

                    <p>
                        general settings information
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <input type="checkbox" id="allow_mail" class="chk-col-grey" >
                    <label for="allow_mail" class="control-sidebar-subheading ">Mail redirect</label>

                    <p>
                        Other sets of options are available
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <input type="checkbox" id="expose_author" class="chk-col-grey" >
                    <label for="expose_author" class="control-sidebar-subheading ">Expose author name</label>

                    <p>
                        Allow the user to show his name in blog posts
                    </p>
                </div>
                <!-- /.form-group -->

                <h3 class="control-sidebar-heading">Chat Settings</h3>

                <div class="form-group">
                    <input type="checkbox" id="show_me_online" class="chk-col-grey" >
                    <label for="show_me_online" class="control-sidebar-subheading ">Show me as online</label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <input type="checkbox" id="off_notifications" class="chk-col-grey" >
                    <label for="off_notifications" class="control-sidebar-subheading ">Turn off notifications</label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        <a href="javascript:void(0)" class="text-red margin-r-5"><i class="fa fa-trash-o"></i></a>
                        Delete chat history
                    </label>
                </div>
                <!-- /.form-group -->
            </form>
        </div>
        <!-- /.tab-pane -->
    </div>
</aside>
<!-- /.control-sidebar -->

<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>

@endsection