@extends('layouts.app')
<!-- Content Wrapper. Contains page content -->
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Пользователи
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Домой</a></li>
            <li class="breadcrumb-item"><a href="#">Примеры</a></li>
            <li class="breadcrumb-item active">Пользователи</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <!-- /.col -->
            <div class="col-lg-12 col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Пользователи</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="employeelist" class="table table-hover no-wrap" data-page-size="10">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Название</th>
                                    <th>Email</th>
                                    <th>Телефон</th>
                                    <th>Роли</th>
                                    <th>Возраст</th>
                                    <th>Дата</th>
                                    <th>Баланс</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>
                                        <a href="profile.html"><img src="{{asset('images/avatar/1.jpg')}}" alt="user" class="avatar avatar-sm mr-5" /> Genelia Deshmukh</a>
                                    </td>
                                    <td>genelia@gmail.com</td>
                                    <td>+123 456 789</td>
                                    <td><span class="label label-danger">Designer</span> </td>
                                    <td>23</td>
                                    <td>12-10-2014</td>
                                    <td>$1200</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-danger-outline" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>
                                        <a href="profile.html"><img src="../../../images/avatar/2.jpg" alt="user" class="avatar avatar-sm mr-5" /> Arijit Singh</a>
                                    </td>
                                    <td>arijit@gmail.com</td>
                                    <td>+234 456 789</td>
                                    <td><span class="label label-info">Developer</span> </td>
                                    <td>26</td>
                                    <td>10-09-2014</td>
                                    <td>$1800</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-danger-outline" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>
                                        <a href="profile.html"><img src="../../../images/avatar/3.jpg" alt="user" class="avatar avatar-sm mr-5" /> Govinda jalan</a>
                                    </td>
                                    <td>govinda@gmail.com</td>
                                    <td>+345 456 789</td>
                                    <td><span class="label label-success">Accountant</span></td>
                                    <td>28</td>
                                    <td>1-10-2013</td>
                                    <td>$2200</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-danger-outline" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>
                                        <a href="profile.html"><img src="../../../images/avatar/4.jpg" alt="user" class="avatar avatar-sm mr-5" /> Hritik Roshan</a>
                                    </td>
                                    <td>hritik@gmail.com</td>
                                    <td>+456 456 789</td>
                                    <td><span class="label label-inverse">HR</span></td>
                                    <td>25</td>
                                    <td>2-10-2017</td>
                                    <td>$200</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-danger-outline" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash" aria-hidden="true"></i></button>
                                    </td>
                                </tr>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="2">
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add-contact">Добавить нового пользователя</button>
                                    </td>
                                    <div id="add-contact" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Add New Contact</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body">
                                                    <from class="form-horizontal form-element">
                                                        <div class="col-md-12 m-b-20">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Type name"> </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Email"> </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Phone"> </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Designation"> </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Age"> </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Date of joining"> </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Salary"> </div>
                                                            <div class="form-group">
                                                                <div class="fileupload btn btn-danger">
                                                                    <div class="file-group">
                                                                        <span><i class="fa fa-camera file-browser mr-10"></i>Upload Contact Image</span>
                                                                        <input type="file">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </from>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-info" data-dismiss="modal">Save</button>
                                                    <button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <td colspan="7">
                                        <div class="text-right">
                                            <ul class="pagination"> </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)">FAQ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.multipurposethemes.com/product/minimal-pro-responsive-bootstrap-4-admin-dashboard-template-webapp-template/">Purchase Now</a>
            </li>
        </ul>
    </div>
    &copy; 2017 <a href="https://www.multipurposethemes.com/">Multi-Purpose Themes</a>. All Rights Reserved.
</footer>
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-light">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-cog fa-spin"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Admin Birthday</h4>

                            <p>Will be July 24th</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-user bg-yellow"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Jhone Updated His Profile</h4>

                            <p>New Email : jhone_doe@demo.com</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Disha Joined Mailing List</h4>

                            <p>disha@demo.com</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-file-code-o bg-green"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Code Change</h4>

                            <p>Execution time 15 Days</p>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Web Design
                            <span class="label label-danger pull-right">40%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-danger" style="width: 40%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Update Data
                            <span class="label label-success pull-right">75%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-success" style="width: 75%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Order Process
                            <span class="label label-warning pull-right">89%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-warning" style="width: 89%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Development
                            <span class="label label-primary pull-right">72%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-primary" style="width: 72%"></div>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
                <h3 class="control-sidebar-heading">General Settings</h3>

                <div class="form-group">
                    <input type="checkbox" id="report_panel" class="chk-col-grey" >
                    <label for="report_panel" class="control-sidebar-subheading ">Report panel usage</label>

                    <p>
                        general settings information
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <input type="checkbox" id="allow_mail" class="chk-col-grey" >
                    <label for="allow_mail" class="control-sidebar-subheading ">Mail redirect</label>

                    <p>
                        Other sets of options are available
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <input type="checkbox" id="expose_author" class="chk-col-grey" >
                    <label for="expose_author" class="control-sidebar-subheading ">Expose author name</label>

                    <p>
                        Allow the user to show his name in blog posts
                    </p>
                </div>
                <!-- /.form-group -->

                <h3 class="control-sidebar-heading">Chat Settings</h3>

                <div class="form-group">
                    <input type="checkbox" id="show_me_online" class="chk-col-grey" >
                    <label for="show_me_online" class="control-sidebar-subheading ">Show me as online</label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <input type="checkbox" id="off_notifications" class="chk-col-grey" >
                    <label for="off_notifications" class="control-sidebar-subheading ">Turn off notifications</label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        <a href="javascript:void(0)" class="text-red margin-r-5"><i class="fa fa-trash-o"></i></a>
                        Delete chat history
                    </label>
                </div>
                <!-- /.form-group -->
            </form>
        </div>
        <!-- /.tab-pane -->
    </div>
</aside>
<!-- /.control-sidebar -->

<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
    @endsection