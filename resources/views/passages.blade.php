@extends('layouts.app')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Проезды
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i>Домой</a></li>
                <li class="breadcrumb-item active">Проезды</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="fa fa-exchange"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">10<small>%</small></span>
                            <span class="info-box-text">Тарифы</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-primary"><i class="fa fa-balance-scale"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">760</span>
                            <span class="info-box-text">Средний Баланс</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fa fa-truck"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">01-07-2018</span>
                            <span class="info-box-text">Первый проезд</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fa fa-car"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">04-07-2018</span>
                            <span class="info-box-text">Проезд Последний</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>

            <div class="row">
                <div class="col-md-6 col-xl-4">
                    <!-- AREA CHART -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Проезды за день</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="areaChart" style="height:400px"></canvas>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

                <div class="col-md-6 col-xl-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h5 class="box-title">Места Проездов</h5>
                            <div class="box-tools pull-right">
                                <ul class="card-controls">
                                    <li class="dropdown">
                                        <a data-toggle="dropdown" href="#"><i class="ion-android-more-vertical"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item active" href="#">Сегодня</a>
                                            <a class="dropdown-item" href="#">Вчера</a>
                                            <a class="dropdown-item" href="#">Прошлая неделя</a>
                                            <a class="dropdown-item" href="#">Прошлый месяц</a>
                                        </div>
                                    </li>
                                    <li><a href="#" class="link card-btn-reload" data-toggle="tooltip" title="" data-original-title="Refresh"><i class="fa fa-circle-thin"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="text-center py-20">
                                <div class="donut" data-peity='{ "fill": ["#7460ee", "#26c6da", "#fc4b6c"], "radius": 145, "innerRadius": 100  }' >9,6,5</div>
                            </div>

                            <ul class="flexbox flex-justified text-center mt-30">
                                <li class="br-1">
                                    <div class="font-size-20 text-primary">953</div>
                                    <small>Варзоб</small>
                                </li>

                                <li class="br-1">
                                    <div class="font-size-20 text-success">813</div>
                                    <small>Хушари</small>
                                </li>

                                <li>
                                    <div class="font-size-20 text-danger">369</div>
                                    <small>Шахристан</small>
                                </li>
                                <li>
                                    <div class="font-size-20 text-danger">569</div>
                                    <small>Истаравшан</small>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>





                <div class="col-xl-4 col-12">
                    <div class="box">
                        <div class="box-body bg-blue">
                            <div class="flexbox mb-20">
                                <h6 class="text-uppercase text-white">Сегодня</h6>
                                <h6 class="text-white"><i class="ion-android-arrow-dropup"></i> %20</h6>
                            </div>
                            <div id="lineToday">1,4,3,7,6,4,8,9,6,8,12</div>
                        </div>

                        <div class="box-body">
                            <ul class="flexbox flex-justified align-items-center">
                                <li class="text-center">
                                    <div class="font-size-20 text-success">%60</div>
                                    <small class="text-uppercase">Прямые тарифы</small>
                                </li>

                                <li class="text-center">
                                    <div class="font-size-20 text-info">%40</div>
                                    <small class="text-uppercase">Онлайн тарифы</small>
                                </li>

                                <li class="text-center">
                                    <div class="font-size-20 text-warning">%40</div>
                                    <small class="text-uppercase">Все тарифы</small>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right d-none d-sm-inline-block">
            <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)">FAQ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://themeforest.net/item/minimal-pro-responsive-bootstrap-4-admin-dashboard-template-webapp-template/21067203">Purchase Now</a>
                </li>
            </ul>
        </div>
        &copy; 2018 <a href="http://irs.tj" target="_blank">IRS</a>. Все права защищены.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-light">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-cog fa-spin"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Admin Birthday</h4>

                                <p>Will be July 24th</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-user bg-yellow"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Jhone Updated His Profile</h4>

                                <p>New Email : jhone_doe@demo.com</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Disha Joined Mailing List</h4>

                                <p>disha@demo.com</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-file-code-o bg-green"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Code Change</h4>

                                <p>Execution time 15 Days</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Web Design
                                <span class="label label-danger pull-right">40%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 40%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Update Data
                                <span class="label label-success pull-right">75%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-success" style="width: 75%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Order Process
                                <span class="label label-warning pull-right">89%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-warning" style="width: 89%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Development
                                <span class="label label-primary pull-right">72%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-primary" style="width: 72%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <input type="checkbox" id="report_panel" class="chk-col-grey" >
                        <label for="report_panel" class="control-sidebar-subheading ">Report panel usage</label>

                        <p>
                            general settings information
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <input type="checkbox" id="allow_mail" class="chk-col-grey" >
                        <label for="allow_mail" class="control-sidebar-subheading ">Mail redirect</label>

                        <p>
                            Other sets of options are available
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <input type="checkbox" id="expose_author" class="chk-col-grey" >
                        <label for="expose_author" class="control-sidebar-subheading ">Expose author name</label>

                        <p>
                            Allow the user to show his name in blog posts
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <h3 class="control-sidebar-heading">Chat Settings</h3>

                    <div class="form-group">
                        <input type="checkbox" id="show_me_online" class="chk-col-grey" >
                        <label for="show_me_online" class="control-sidebar-subheading ">Show me as online</label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <input type="checkbox" id="off_notifications" class="chk-col-grey" >
                        <label for="off_notifications" class="control-sidebar-subheading ">Turn off notifications</label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            <a href="javascript:void(0)" class="text-red margin-r-5"><i class="fa fa-trash-o"></i></a>
                            Delete chat history
                        </label>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

@endsection