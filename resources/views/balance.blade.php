@extends('layouts.app')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Пополнения баланса
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i>Домой</a></li>
            <li class="breadcrumb-item"><a href="#">Детализация</a></li>
            <li class="breadcrumb-item active">Пополнения баланса</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xl-6 col-md-12 col">
                <div class="info-box">
                    <span class="info-box-icon bg-info"><i class="fa fa-money"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-number">100 <small>сомони</small></span>
                        <span class="info-box-text">Текущий Баланс</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>


            <!-- /.col -->
            <div class="col-xl-6 col-md-12 col">
                <div class="info-box">
                    <span class="info-box-icon bg-danger"><i class="fa fa-exchange"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-number">10 <small>%</small></span>
                        <span class="info-box-text">Тариф за каждый проезд</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>


            <!-- /.c

<!-- Main content -->
    <section class="invoice printableArea">
        <!-- title row -->
        <div class="col-lg-12 col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Пополнения Баланса</h3>
                </div>
                <div class="box-body">
                    <!-- Date -->
                    <div class="form-group">
                        <label>Держатель карты:</label>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="client_id">
                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- /.form group -->

                    <!-- Date range -->
                    <div class="form-group">
                        <label>Тип платежа:</label>
                        <div class="input-group">
                            <div class="col-12 col-sm-6">
                                <img src="../../../images/visa.png" alt="Visa">
                                <img src="../../../images/mastercard.png" alt="Mastercard">
                                <img src="../../../images/american-express.png" alt="American Express">
                                <img src="../../../images/paypal2.png" alt="Paypal">
                            </div>
                        </div>
                    </div>

                        <!-- /.input group -->
                    <!-- /.form group -->

                    <!-- Date and time range -->
                    <div class="form-group">
                        <label>Дата и время:</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="datetime">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Сумма пополнения:</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-money"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="amount">
                        </div>
                        <!-- /.input group -->
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Пополнить</button>
                    </div>

                    <!-- /.form group -->


                    <!-- /.form group -->

                </div>
                <!-- /.box-body -->
            </div>

            <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Тип платежа</th>
                        <th>RRN</th>
                        <th class="text-right">Дата</th>
                        <th class="text-right">Держатель карты</th>
                        <th class="text-right">Баланс</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>NPS</td>
                        <td>12345678912514</td>
                        <td class="text-right">16-07-2018</td>
                        <td class="text-right">A3456789</td>
                        <td class="text-right">100 сомони</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Банкомат</td>
                        <td>45668668868686</td>
                        <td class="text-right">15-07-2018</td>
                        <td class="text-right">A2567981</td>
                        <td class="text-right">1500 сомони</td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-12">
                <button id="print" class="btn btn-warning" type="button"> <span><i class="fa fa-print"></i> Печатать</span> </button>
                <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Потвердить платеж
                </button>
                <button type="button" class="btn btn-danger pull-right" style="margin-right: 5px;">
                    <i class="fa fa-download"></i> Генерировать PDF файл
                </button>
            </div>
        </div>
    </section>
        </div>
    <!-- /.content -->
    <div class="clearfix"></div>
</div>
<!-- /.content-wrapper -->
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)">FAQ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.multipurposethemes.com/product/minimal-pro-responsive-bootstrap-4-admin-dashboard-template-webapp-template/">Purchase Now</a>
            </li>
        </ul>
    </div>
    &copy; 2018 <a href="http://irs.tj" target="_blank">IRS</a>. Все права защищены.
</footer>

<!-- /.control-sidebar -->

<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
    @endsection