@extends('layouts.app')
    @section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Настройки профиля
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i>Домой</a></li>
                <li class="breadcrumb-item"><a href="#">Детализация</a></li>
                <li class="breadcrumb-item active">Настройки профиля</li>
            </ol>
        </section>

        <section class="content">

            <div class="row">
                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="fa fa-exchange"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">10<small>%</small></span>
                            <span class="info-box-text">Тарифы</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-primary"><i class="fa fa-balance-scale"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">760</span>
                            <span class="info-box-text">Средний Баланс</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fa fa-truck"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">01-07-2018</span>
                            <span class="info-box-text">Первый проезд</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-xl-3 col-md-6 col">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fa fa-car"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">04-07-2018</span>
                            <span class="info-box-text">Проезд Последний</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
            <div class="box">
            <form class="form-horizontal form-element col-12">
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 control-label" style="margin-top: 20px;">Пароль</label>

                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputPassword" placeholder="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword1" type="password" class="col-sm-2 control-label" style="margin-top: 20px;">Потвердить пароль</label>

                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputPassword1" placeholder="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="ml-auto col-sm-10">
                        <div class="checkbox">
                            <input type="checkbox" id="basic_checkbox_1" checked="">
                            <label for="basic_checkbox_1"> Я согласен с </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;<a href="#">условиями соглашения</a>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="ml-auto col-sm-10">
                        <button type="submit" class="btn btn-success">Изменить</button>
                    </div>
                </div>
            </form>
            </div>
        </section>
    </div>
    <footer class="main-footer">
        <div class="pull-right d-none d-sm-inline-block">
            <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)">FAQ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://themeforest.net/item/minimal-pro-responsive-bootstrap-4-admin-dashboard-template-webapp-template/21067203">Purchase Now</a>
                </li>
            </ul>
        </div>
        &copy; 2018 <a href="http://irs.tj" target="_blank">IRS</a>. Все права защищены.
    </footer>

    @endsection