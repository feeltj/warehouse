<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../images/favicon.ico">

    <title>IRS вход в личный кабинет</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../bootstrap-4/admin/minimalproadmin/assets/vendor_components/bootstrap/dist/css/bootstrap.css">

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="../bootstrap-4/admin/minimalproadmin/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="../css/master_style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <!-- MinimalPro Admin Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../css/skins/_all-skins.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <span class="login100-form-avatar">
        <img src="../bootstrap-4/admin/minimalproadmin/images/aries-light1.png" style="margin-top:10px;">
        </span>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body pb-20">
        <p class="login-box-msg text-uppercase">Восстановить Пароль</p>

        <form method="POST" action="{{ route('password.email') }}" class="form-element">
            @csrf
            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>
                <span class="ion ion-email form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="row">
                <!-- /.col -->
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-info btn-block text-uppercase">Сбросить</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<!-- jQuery 3 -->
<script src="../bootstrap-4/admin/minimalproadmin/assets/vendor_components/jquery/dist/jquery.min.js"></script>
<script src="../bootstrap-4/admin/minimalproadmin/assets/vendor_components/popper/dist/popper.min.js"></script>
<script src="../bootstrap-4/admin/minimalproadmin/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>