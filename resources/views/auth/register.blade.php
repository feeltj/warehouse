@extends('layouts.app_header_auth')
@section('content')
<div class="register-box">
    <div class="register-logo">
       <span class="login100-form-avatar">
      <img src="bootstrap-4/admin/minimalproadmin/images/aries-light1.png" style="margin-top:10px;">
  </span>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg">Регистрация нового пользователя</p>

        <form method="POST" action="{{ route('register') }}" class="form-element">
            @csrf
            <div class="form-group has-feedback">
                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Имя" required autofocus>
                <span class="ion ion-person form-control-feedback "></span>
                @if ($errors->has('name'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>
                <span class="ion ion-email form-control-feedback "></span>
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Пароль" required>
                <span class="fas fa-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="password-confirm" type="password" class="form-control" placeholder="Повторить пароль" name="password_confirmation" required>
                <span class="fas fa-sign-in-alt form-control-feedback "></span>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="checkbox">
                        <input type="checkbox" id="basic_checkbox_1" >
                        <label for="basic_checkbox_1">Я согласен <a href="#"><b>с Условиями</b></a></label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-info btn-block margin-top-10">Регистрация</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <!-- /.social-auth-links -->

        <div class="margin-top-20 text-center">
            <p>Уже есть аккаунт?<a href="{{ route('login') }}" class="text-info m-l-5"> Войти</a></p>
        </div>

    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->

@endsection