@extends('layouts.app_header_auth')

@section('content')

    <div class="login-page_bg">
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper" style="">


            <div class="animate form login_form">
                <section class="login_content">
                    <div class="login-logo">
  <span class="login100-form-avatar">
      <img src="../../img/aries-light1.png" style="margin-top:10px;">
  </span>

                    </div>
                    <div class="login_b">
                    <form  method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf
                        <h1>Вход в Склад</h1>
                        <div>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Пароль">

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div>
                            <button type="submit" class="btn btn-default submit">Войти</button>
                            <a class="reset_pass" href="{{ route('password.request') }}">Забыли пароль?</a>
                        </div>

                        <div class="clearfix"></div>


                    </form>
                    </div>
                </section>
            </div>


        </div>

</div>
@endsection
