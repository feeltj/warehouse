@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-folder-o'></i> Редактировать Линию</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('lines.update',$line->id) }}" method="post" >
            <input type="hidden" name="_method" value="PUT">
            @csrf


            <div class="form-group">
                <label for="name">Номер Линии</label>
                <input type="text" name="value" class="form-control"  value="{{ $line->value }}" required>
            </div><br>
            <div class="form-group">
                <label>Категория родительская</label>
                <select class="form-control" name="category_id" id="category">
                    <!--<option value=""></option>-->
                    @foreach($categories as $category)

                        <option value="{{$category->id}}" {{ $category->id == $line->category_id ? "selected" : "" }}>{{ $category->name }}</option>

                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Изменить</button>

        </form>
    </div>


@endsection