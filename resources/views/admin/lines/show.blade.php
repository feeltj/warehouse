@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-folder-o'></i>{{ $line->value }}</h3>
        <br>





            <div class="form-group">
                <label for="name">Номер Линии</label>
                <input type="text" name="value" class="form-control"  value="{{ $line->value }}" disabled>
            </div><br>
            <div class="form-group">
                <label>Категория родительская</label>
                <select class="form-control" name="category_id" id="category" disabled>
                    <!--<option value=""></option>-->
                    @foreach($categories as $category)

                        <option value="{{$category->id}}" {{ $category->id == $line->category_id ? "selected" : "" }}>{{ $category->name }}</option>

                    @endforeach
                </select>
            </div>

    </div>


@endsection