@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class="fa fa-exclamation"></i> {{ $equiptstatus->name }}</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif



            <div class="form-group">
                <label for="name">Название Статуса</label>
                <input type="text" name="name" class="form-control" value="{{ $equiptstatus->name }}" placeholder="Название статуса" disabled>
            </div><br>

    </div>


@endsection