@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-folder-o'></i> {{ $finalcategory->name }}</h3>
        <br>


        <div class="form-group">
            <label>Подкатегория</label>
            <select class="form-control" name="subcategory_id" id="subcategory" disabled>
                <!--<option value=""></option>-->
                @foreach($subcategories as $subcategory)

                    <option value="{{$subcategory->id}}" {{ $subcategory->id == $finalcategory->subcategory_id ? "selected" : "" }}>{{ $subcategory->name }}</option>

                @endforeach
            </select>
        </div>
        <!--<div class="form-group">
            <label>Подкатегории
                <select class="form-control" name="subcategory" id="subcategory">
                    <option value=""></option>
                </select>
            </label>
        </div>-->
        <div class="form-group">
            <label for="name">Название Финальной категории</label>
            <input type="text" name="name" value="{{ $finalcategory->name }}" class="form-control" placeholder="Название категории" disabled>
        </div><br>

    </div>
    <!-- <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script>
        $('#category').on('change', function(e){
            console.log(e);
            var cat_id = e.target.value;

            $.get('{{ url('admin/information') }}/create/ajax-state?category_id=' + cat_id, function(data) {
                //console.log(data);
                $('#subcategory').empty();
                $.each(data, function(index,subCatObj){
                    //console.log(subCatObj.name);
                    $('#subcategory').append('<option value="'+subCatObj.id+'">'+subCatObj.name+'</option>')
                });

            });
        });
    </script>-->

@endsection