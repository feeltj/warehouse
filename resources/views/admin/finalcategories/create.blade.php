@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-folder-o'></i> Добавить Финальную категорию</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('finalcategories.store') }}" method="post" >
            @csrf
            <div class="form-group">
                <label>Подкатегория</label>
                <select class="form-control" name="subcategory_id" id="subcategory">
                    <option>Выбрать под категорию</option>
                    @foreach($subcategories as $subcategory)

                        <option value="{{$subcategory->id}}">{{ $subcategory->name }}</option>
                    @endforeach
                </select>
            </div>
            <!--<div class="form-group">
                <label>Подкатегории
                    <select class="form-control" name="subcategory" id="subcategory">
                        <option value=""></option>
                    </select>
                </label>
            </div>-->
            <div class="form-group">
                <label for="name">Название Финальной категории</label>
                <input type="text" name="name" class="form-control" placeholder="Название категории" required>
            </div><br>
            <button type="submit" class="btn btn-primary">Добавить</button>

        </form>
    </div>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $('#subcategory').select2({
            closeOnSelect: true
        });


    </script>
    <!--<script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script>
        $('#category').on('change', function(e){
            console.log(e);
            var cat_id = e.target.value;

            $.get('{{ url('admin/information') }}/create/ajax-state?category_id=' + cat_id, function(data) {
                //console.log(data);
                $('#subcategory').empty();
                $.each(data, function(index,subCatObj){
                    //console.log(subCatObj.name);
                    $('#subcategory').append('<option value="'+subCatObj.id+'">'+subCatObj.name+'</option>')
                });

            });
        });
    </script>-->

@endsection