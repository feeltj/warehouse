@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">


                <h3><i class='fa fa-key'></i> Редактировать Роль: {{$role->name}}</h3>
            <hr>
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error  }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('roles.update',$role->id) }}" method="post">
                <input type="hidden" name="_method" value="PUT">
                @csrf
                <div class="form-group">
                    <label for="name">Название Роли</label>
                    <input type="text" value="{{$role->name}}" class="form-control" id="name"  name="name" >
                </div>

            <h5><b>Assign Permissions</b></h5>
            <select class="form-control" name="permissions[]" multiple>
                @foreach ($permissions as $permission)

                    <option value="{{ $permission->id }}">{{ $permission->name }}</option>


                @endforeach
            </select>
            <br>
                <button type="submit" class="btn btn-primary">Редактировать</button>

            </form>
        </div>

    @endsection