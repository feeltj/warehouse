@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
                <h3><i class='fa fa-key'></i> Добавить Роль</h3>
            <hr>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error  }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('roles.store') }}" method="post" >
                        @csrf
                        <div class="form-group">

                            <input type="text" name="name" class="form-control" placeholder="Название Роли" required>
                        </div>

            <h5><b>Привязать Права Доступа</b></h5>

            <div class='form-group'>
                <select class="form-control" name="permissions[]" multiple>
                @foreach ($permissions as $permission)

                    <option value="{{ $permission->id }}">{{ $permission->name }}</option>


                @endforeach
                </select>
            </div>

                        <button type="submit" class="btn btn-primary">Добавить Роль</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
    @endsection