@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">

        <form action="{{ route('stocks.add', $stock->id) }}" method="post" >
        <div class="form-group">
            <label for="name">Название</label>

            <select class="form-control">
                @foreach($equiptments as $equiptment)
                    @if($stock->equiptment_id == $equiptment->id)
                <option value="{{  $stock->equiptment_id }}" {{ $equiptment->id ? "selected" : ""  }} >{{  $equiptment->name }}</option>
                    @endif
                    @endforeach
            </select>
        </div><br>

        <div class="form-group">
            <label for="name">Категория</label>
            <select class="form-control" name="category_id" id="category">

                @foreach($categories as $category)
                    @foreach($equiptments as $equiptment)
                        @if($stock->equiptment_id == $equiptment->id)
                            <option value="{{ $category->id }}" {{ $equiptment->category_id == $category->id ? "selected" : ""  }} >{{ $category->name }}</option>
                        @endif
                    @endforeach
                @endforeach
            </select>
        </div><br>

        <div class="form-group">
            <label for="name">Подкатегория</label>
            <select class="form-control" name="subcategory_id" id="subcategory">

                @foreach($subcategories as $subcategory)
                    @foreach($equiptments as $equiptment)
                        @if($stock->equiptment_id == $equiptment->id)
                            <option value="{{ $subcategory->id }}" {{ $equiptment->subcategory_id == $subcategory->id ? "selected" : ""  }}>{{ $subcategory->name }}</option>
                        @endif
                    @endforeach
                @endforeach
            </select>
        </div><br>
        <div class="form-group" id="line_num">
            <label for="name">Линия</label>
            <select class="form-control" name="line_id" id="line">
                @foreach($lines as $line)

                    @if($stock->line_id == $line->id)
                        <option value="{{ $line->id }}" {{ $stock->line_id == $line->id ? "selected" : ""  }}>{{ $line->value }}</option>
                    @endif

                @endforeach
            </select>
        </div><br>
        <div class="form-group">
            <label for="name">Материально ответственный</label>
            <select class="form-control" name="user_id" id="user">
                @foreach($users as $user)
                    <option value="{{ $user->id }}"  {{ $stock->user_id == $user->id ? "selected" : ""  }} >{{ $user->name }}</option>
                @endforeach
            </select>
        </div><br>

        <div class="form-group">
            <div class="form-group">
                <label>Состояние оборудования</label>
                <select class="form-control" name="equiptstatus_id" id="equiptstatus">
                    @foreach($equiptstatuses as $equiptstatus)
                        <option value="{{ $equiptstatus->id }}" {{ $stock->equiptstatus_id == $equiptstatus->id ? "selected" : ""  }}>{{ $equiptstatus->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="name">Регионы</label>
            <select class="form-control" name="region_id" id="region">
                @foreach($regions as $region)
                    <option value="{{ $region->id }}" {{ $stock->region_id == $region->id ? "selected" : ""  }} >{{ $region->name }}</option>
                @endforeach
            </select>
        </div><br>

        <div class="form-group">
            <label for="name">Склады</label>
            <select class="form-control" name="warehouse_id" id="warehouse">
                @foreach($warehouses as $warehouse)
                    <option value="{{ $warehouse->id }}" {{ $stock->warehouse_id == $warehouse->id ? "selected" : ""  }}>{{ $warehouse->name }}</option>
                @endforeach
            </select>
        </div><br>
        <div class="form-group">
            {!! Form::submit('Добавить', ['class'=>'btn btn-primary']) !!}
        </div>
        <form action="{{ route('stocks.add', $stock->id) }}" method="post" class="form-inline">
            @csrf
            <div class="form-group mb-2">
                        <label>Названия действия</label>
                      <textarea name="description" class="form-control"></textarea>
              </div>
            <div class="form-group mb-2">
                <label>Статус До</label>
                <input type="text"  name="value_before" class="form-control" >
            </div>

            <div class="form-group mb-2">
                <label>Статус После</label>
                <input type="text"  name="value_after" class="form-control" >
            </div>
            <div class="form-group mb-2">
                {!! Form::submit('Добавить', ['class'=>'btn btn-primary']) !!}
            </div>
        </form>
            <div class="x_panel">
                <h4><i class="fa fa-bar-chart"></i> История Работ по Оборудованию </h4>
                <hr>
        <table class="table table-responsive">
            <thead>
            <th>
                ID
            </th>
            <th>
                Описание
            </th>
            <th>
              Состояние До
            </th>
            <th>
                Состояние После
            </th>
            <th>
                Дата Изменения
            </th>
            </thead>
            @foreach($repairs as $repair)
            <tbody>
            @if($stock->id == $repair->stock_id)
            <td>
                {{ $repair->id }}
            </td>
            <td>
                {{ $repair->description }}
            </td>
            <td>
                {{ $repair->value_before }}
            </td>
            <?php
            setlocale(LC_ALL, 'ru_RU.UTF-8');
            ?>
            <td> {{ $repair->value_after }}</td>
            <td> {{ $repair->updated_at->formatLocalized('%d %B %Y, %I:%M:%S %p') }}</td>
                @endif
            </tbody>
                    @endforeach
        </table>
            </div>
        </form>
    </div>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>

        $('#subcategory').select2({
            closeOnSelect: true
        });
        $('#line').select2({
            closeOnSelect: true
        });
        $('#user').select2({
            closeOnSelect: true
        });
        $('#equiptstatus').select2({
            closeOnSelect: true
        });

        $('#warehouse').select2({
            closeOnSelect: true
        });
    </script>
    <script>
        $('#category').on('click', function(e){
            //console.log(e);
            var cat_id = e.target.value;
            console.log($('#category').val());

            //if(cat_id == 10)
            //{
            //$('#line_num').show();
            $.get('{{ url('admin/information') }}/create?category_id=' + cat_id, function(data) {
                console.log(data);

                $('#line').empty();
                $('#subcategory').empty();
                $('#finalcategory').empty();
                $.each(data, function(index,subCatObj){
                    //$('#line').removeAttr('disabled');
                    //console.log(subCatObj.name);

                    $('#line').append('<option value="'+subCatObj.id+'">'+subCatObj.value+'</option>')
                });

            });
            //}
            //else {
            //$('#line_num').hide();
            $.get('{{ url('admin/information') }}/create/ajax-state?category_id=' + cat_id, function (data) {
                //console.log(data);
                $('#subcategory').empty();

                $.each(data, function (index, subCatObj) {
                    // $('#line').prop('disabled', true);
                    //console.log(subCatObj.name);
                    $('#subcategory').append('<option value="' + subCatObj.id + '">' + subCatObj.name + '</option>')
                });

            });
            //}
        });

        $('#region').on('click', function(e){
            console.log(e);
            var region_id = e.target.value;

            $.get('{{ url('admin/information') }}/create/ajax?region_id=' + region_id, function(data) {
                //console.log(data);
                $('#warehouse').empty();
                $.each(data, function(index,subCatObj){
                    //console.log(subCatObj.name);
                    $('#warehouse').append('<option value="'+subCatObj.id+'">'+subCatObj.name+'</option>')
                });

            });
        });
    </script>
@endsection