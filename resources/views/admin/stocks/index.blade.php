@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h2><i class="fa fa-institution"></i> Список Оборудований </h2>
                    <hr>

                    <div class="x_content">
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))

                                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                @endif
                            @endforeach
                        </div> <!-- end .flash-message -->
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Название Оборудования</th>
                                <th>Материально Ответственный</th>
                                <th>Статус</th>
                                <th>Дата </th>
                                <th>Действия</th>

                            </tr>
                            </thead>


                            <tbody>
                            @foreach ($stocks as $stock)
                            <tr>





                                    <td>{{ $stock->id }}</td>
                                                @foreach($equiptments as $equiptment)
                                                    @if($stock->equiptment_id == $equiptment->id)
                                    <td><a href="{{ route('stocks.show',$stock->id) }}">{{ $equiptment->name }}</a></td>
                                                    @endif
                                                @endforeach

                                            @foreach($users as $user)
                                                @if( $stock->user_id == $user->id)
                                    <td>{{ $user->name }}</td>


                                                @endif

                                            @endforeach

                                        @foreach($equiptstatuses as $equiptstatus)
                                            @if($stock->equiptstatus_id == $equiptstatus->id)
                                    <td>{{ $equiptstatus->name }}</td>
                                    @elseif($stock->equiptstatus_id == null)

                                            @endif
                                        @endforeach
                                <?php
                                setlocale(LC_ALL, 'ru_RU.UTF-8');
                                ?>
                                @if($stock->equiptstatus_id == null)
                                <td></td>
                                @endif
                                @if($stock->user_id == null)
                                    <td></td>
                                @endif
                                <td>{{  $stock->created_at->formatLocalized('%d %B %Y, %I:%M:%S %p') }}</td>


                                    {{-- Retrieve array of roles associated to a user and convert to string --}}
                                    <td>
                                        <a href="{{ route('stocks.show', $stock->id) }}" class="btn  btn-success"  style="margin-right: 3px;">Подрбнее</a>
                                        <a href="{{ route('stocks.edit', $stock->id) }}" class="btn  btn-primary"  style="margin-right: 3px;">Редактировать</a>



                                        <form action="{{ route('stocks.destroy', $stock->id) }}" method="post" class="btn-group inline">
                                            {{method_field('DELETE')}}
                                            @csrf
                                            <button type="submit" class="btn btn-danger" style="position: relative;bottom:2px">Удалить</button>
                                        </form>

                                    </td>
                                </tr>








                                    @endforeach

                            </tbody>
                        </table>
                        {{ $stocks->links() }}
                    </div>
                    <!--<a href="{{ route('stocks.create') }}" class="btn btn-success">Добавить материально ответственного</a>-->
                </div>

            </div>

        </div>
    </div>

    @endsection