@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-institution'></i> Редактировать Склад</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('stocks.update', $stock->id) }}" method="post" >
            <input type="hidden" name="_method" value="PUT">
            @csrf
            <div class="form-group">
                <label>Наименование</label>
                    @foreach($equiptments as $equiptment)
                    @if($stock->equiptment_id == $equiptment->id)
                        <input type="text" name="name" value="{{  $equiptment->name}}" class="form-control" placeholder=""  disabled>
                    @endif
                    @endforeach
            </div>

            <div class="form-group">
              <label for="description">Описание</label>
               <textarea class="form-control" name="description">{{$stock->description}}</textarea>
            </div>
            <div class="form-group">
                <label for="name">Категория</label>
                <select class="form-control" name="category_id" id="category">

                    @foreach($categories as $category)
                        @foreach($equiptments as $equiptment)
                            @if($stock->equiptment_id == $equiptment->id)
                        <option value="{{ $category->id }}" {{ $equiptment->category_id == $category->id ? "selected" : ""  }} >{{ $category->name }}</option>
                            @endif
                        @endforeach
                    @endforeach
                </select>
            </div><br>

            <div class="form-group">
                <label for="name">Подкатегория</label>
                <select class="form-control" name="subcategory_id" id="subcategory">

                    @foreach($subcategories as $subcategory)
                        @foreach($equiptments as $equiptment)
                            @if($stock->equiptment_id == $equiptment->id)
                        <option value="{{ $subcategory->id }}" {{ $equiptment->subcategory_id == $subcategory->id ? "selected" : ""  }}>{{ $subcategory->name }}</option>
                            @endif
                        @endforeach
                    @endforeach
                </select>
            </div><br>
            <div class="form-group" id="line_num">
                <label for="name">Линия</label>
                <select class="form-control" name="line_id" id="line">
                    @foreach($lines as $line)

                        @if($stock->line_id == $line->id)
                            <option value="{{ $line->id }}" {{ $stock->line_id == $line->id ? "selected" : ""  }}>{{ $line->value }}</option>
                        @endif

                    @endforeach
                </select>
            </div><br>

            <div class="form-group">
                <label>Материально Ответственный</label>
                <select class="form-control" name="user_id" id="user">
                    @foreach($users as $user)
                        <option value="{{ $user->id }}" {{ $stock->user_id == $user->id ? "selected" : ""  }}>{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label>Статус оборудования</label>
                    <select class="form-control" name="equiptstatus_id" id="equiptstatus">
                        @foreach($equiptstatuses as $equiptstatus)
                            <option value="{{ $equiptstatus->id }}" {{ $stock->equiptstatus_id == $equiptstatus->id ? "selected" : ""  }}>{{ $equiptstatus->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="form-group">
                    <label>Регион</label>
                    <select class="form-control" name="region_id" id="region">
                        @foreach($regions as $region)
                            <option value="{{ $region->id }}" {{ $stock->region_id == $region->id ? "selected" : ""  }} >{{ $region->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label>Склад</label>
                    <select class="form-control" name="warehouse_id" id="warehouse">
                        @foreach($warehouses as $warehouse)
                            <option value="{{ $warehouse->id }}" {{ $stock->warehouse_id == $warehouse->id ? "selected" : ""  }}>{{ $warehouse->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                {!! Form::submit('Добавить', ['class'=>'btn btn-primary']) !!}
            </div>

        </form>
    </div>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>

        $('#subcategory').select2({
            closeOnSelect: true
        });
        $('#line').select2({
            closeOnSelect: true
        });
        $('#user').select2({
            closeOnSelect: true
        });
        $('#equiptstatus').select2({
            closeOnSelect: true
        });
        $('#region').select2({
            closeOnSelect: true
        });

        $('#warehouse').select2({
            closeOnSelect: true
        });
    </script>
    <script>
        $('#category').on('click', function(e){
            //console.log(e);
            var cat_id = e.target.value;
            console.log($('#category').val());

            //if(cat_id == 10)
            //{
            //$('#line_num').show();
            $.get('{{ url('admin/information') }}/create?category_id=' + cat_id, function(data) {
                console.log(data);

                $('#line').empty();
                $('#subcategory').empty();
                $('#finalcategory').empty();
                $.each(data, function(index,subCatObj){
                    //$('#line').removeAttr('disabled');
                    //console.log(subCatObj.name);

                    $('#line').append('<option value="'+subCatObj.id+'">'+subCatObj.value+'</option>')
                });

            });
            //}
            //else {
            //$('#line_num').hide();
            $.get('{{ url('admin/information') }}/create/ajax-state?category_id=' + cat_id, function (data) {
                //console.log(data);
                $('#subcategory').empty();

                $.each(data, function (index, subCatObj) {
                    // $('#line').prop('disabled', true);
                    //console.log(subCatObj.name);
                    $('#subcategory').append('<option value="' + subCatObj.id + '">' + subCatObj.name + '</option>')
                });

            });
            //}
        });

        $('#region').on('change', function(e){
            console.log(e);
            var region_id = e.target.value;

            $.get('{{ url('admin/information') }}/create/ajax?region_id=' + region_id, function(data) {
                //console.log(data);
                $('#warehouse').empty();
                $.each(data, function(index,subCatObj){
                    //console.log(subCatObj.name);
                    $('#warehouse').append('<option value="'+subCatObj.id+'">'+subCatObj.name+'</option>')
                });

            });
        });
    </script>
@endsection