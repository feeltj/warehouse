@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-institution'></i> Добавить Склад</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
             @endif
        <form action="{{ route('stocks.store') }}" method="post" >
            @csrf
        <div class="form-group">
           <label>Оборудование</label>
         <select class="form-control" name="equiptment_id" id="equiptment">
             @foreach($equiptments as $equiptment)
                 <option value="{{ $equiptment->id }}">{{ $equiptment->name }}</option>
                 @endforeach
         </select>
        </div>

            <div class="form-group">
                {!! Form::label('description', 'Описание:') !!}
                {!! Form::text('description', null, ['class'=>'form-control'])!!}
            </div>
        <div class="form-group">
            <label>Пользователи</label>
            <select class="form-control" name="user_id">
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <div class="form-group">
                <label>Статус оборудования</label>
                <select class="form-control" name="equiptstatus_id">
                    @foreach($equiptstatuses as $equiptstatus)
                        <option value="{{ $equiptstatus->id }}">{{ $equiptstatus->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

            <div class="form-group">
                <div class="form-group">
                    <label>Регион</label>
                    <select class="form-control" name="region_id">
                        @foreach($regions as $region)
                            <option value="{{ $region->id }}">{{ $region->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        <div class="form-group">
            {!! Form::submit('Add Stock', ['class'=>'btn btn-primary']) !!}
        </div>

        </form>
    </div>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $('#equiptment').select2({
        closeOnSelect: true
    });
    </script>
    @endsection