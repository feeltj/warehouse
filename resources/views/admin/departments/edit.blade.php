@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-building'></i> Редактировать Отдел</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('departments.update', $department->id) }}" method="post" >
            <input type="hidden" name="_method" value="PUT">
            @csrf

            <div class="form-group">
                <label for="name">Название Отдела</label>
                <input type="text" name="name" value="{{ $department->name }}" class="form-control" placeholder="Название отдела" required>
            </div><br>
            <button type="submit" class="btn btn-primary">Измениить</button>

        </form>
    </div>


@endsection