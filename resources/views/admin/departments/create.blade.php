@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-building'></i> Добавить Отдел</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('departments.store') }}" method="post" >
            @csrf

            <div class="form-group">
                <label for="name">Название Отдела</label>
                <input type="text" name="name" class="form-control" placeholder="Название отдела" required>
            </div><br>
            <button type="submit" class="btn btn-primary">Добавить</button>

        </form>
    </div>


@endsection