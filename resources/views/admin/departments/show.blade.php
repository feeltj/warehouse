@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class="fa fa-building"></i>{{ $department->name }}</h3>
        <br>



            <div class="form-group">
                <label for="name">Название Отдела</label>
                <input type="text" name="name" value="{{ $department->name }}" class="form-control" placeholder="Название отдела" disabled>
            </div><br>
            <button type="submit" class="btn btn-primary">Измениить</button>

    </div>


@endsection