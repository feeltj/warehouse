@extends('layouts.app_header_admin')
@section('content')
@section('styles')

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">
@endsection
<!-- Content Wrapper. Contains page content -->
<!-- /top navigation -->
<div class="right_col" role="main">
    <!-- page content -->

    <!-- /top tiles -->

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h2><i class="	fa fa-bar-chart"></i> Отчеты </h2>
                    <hr>
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">Отчеты Оборудований</a></li>
                        <li><a data-toggle="tab" href="#menu1">Электронный Документ</a></li>
                        <li><a data-toggle="tab" href="#menu2">Cклад Оборудований</a></li>
                        <li><a data-toggle="tab" href="#menu3">Отчет Состояний</a></li>
                    </ul>
                    <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
            <table class="table" id="example" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>
                        Названия Оборудования
                    </th>
                    <th>
                        Материально Ответсвенные
                    </th>
                    <th>
                        Количество Оборудований
                    </th>
                </tr>
                </thead>


                <tbody>
                @foreach($stock_amount as $amount)
                    @foreach($users as $user)

                        @if( $amount->user_id == $user->id)
                            <tr>
                                <td>{{ $amount->name }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $amount->equiptment_id}}</td>
                            </tr>

                        @endif
                    @endforeach
                @endforeach
                </tbody>

            </table>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <div class="x_content">

                            <table id="example1" class="table">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Наименование</th>
                                    <th>Категория</th>
                                    <th>Количество</th>
                                    <th>Дата</th>
                                </tr>
                                </thead>


                                <tbody>
                                @foreach ($equiptments as $equiptment)
                                    <tr>
                                        <td>{{ $equiptment->id }}</td>
                                        <td> <a href="{{ route('equiptments.show',$equiptment->id) }}">{{$equiptment->name }}</a></td>
                                        @foreach($subcategories as $subcategory)
                                            @if( $equiptment->subcategory_id == $subcategory->id)
                                                <td>{{ $subcategory->name }}</td>

                                            @endif
                                        @endforeach


                                        <td>{{ $equiptment->quantity }}</td>
                                        <?php
                                        setlocale(LC_ALL, 'ru_RU.UTF-8');
                                        ?>
                                        <td>{{  $equiptment->created_at->formatLocalized('%d %B %Y, %I:%M:%S %p') }}</td>
                                        {{-- Retrieve array of roles associated to a user and convert to string --}}
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>


                        </div>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <div class="x_content">

                            <table id="example2" class="table">
                                <thead>
                                <tr>
                                    <th>Наименование</th>
                                    <th>Количество Оборудований</th>

                                </tr>
                                </thead>


                                <tbody>
                                @foreach($stock_w as $a)


                                            <tr>
                                                <td>{{ $a->name }}</td>
                                                <td>{{ $a->equiptment_id}}</td>
                                            </tr>

                                @endforeach

                                </tbody>
                            </table>


                        </div>
                    </div>
                    <div id="menu3" class="tab-pane fade">
                        <div class="x_content">

                            <table id="example3" class="table">
                                <thead>
                                <tr>
                                    <th>Состояние Оборудования</th>
                                    <th>Количество Оборудований</th>

                                </tr>
                                </thead>


                                <tbody>
                                @foreach($stock_s as $s)


                                    <tr>
                                        <td>{{ $s->name }}</td>
                                        <td>{{ $s->equiptment_id}}</td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                    </div>
                    </div>

        </div>
        </div>
    </div>
        @section('javascripts')
        <script src="
https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
            <script>
                $(document).ready(function() {
                    var table = $('#example').DataTable( {
                        lengthChange: false,
                        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
                    } );

                    table.buttons().container()
                        .appendTo( '#example_wrapper .col-sm-6:eq(0)' );

                    var table_1 = $('#example1').DataTable( {
                        lengthChange: false,
                        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
                    } );

                    table_1.buttons().container()
                        .appendTo( '#example1_wrapper .col-sm-6:eq(0)' );


                    var table_2 = $('#example2').DataTable( {
                        lengthChange: false,
                        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
                    } );

                    table_2.buttons().container()
                        .appendTo( '#example2_wrapper .col-sm-6:eq(0)' );


                    var table_3 = $('#example3').DataTable( {
                        lengthChange: false,
                        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
                    } );

                    table_3.buttons().container()
                        .appendTo( '#example3_wrapper .col-sm-6:eq(0)' );
                    //$('#example').DataTable( {
                    //} );
                    //$('#datatable').DataTable( {
                    //} );
                } );
            </script>
@endsection
@endsection