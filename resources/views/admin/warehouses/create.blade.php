@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-institution'></i> Добавить Склад</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('warehouses.store') }}" method="post" >
            @csrf

            <div class="form-group">
                <label for="name">Название Склада</label>
                <input type="text" name="name" class="form-control" placeholder="Название склада" required>
            </div><br>
            <div class="form-group">
                <label for="name">Регион</label>
                <select class="form-control" name="region_id" id="region">
                    @foreach($regions as $region)
                        <option value="{{$region->id}}">{{ $region->name }}</option>
                    @endforeach
                </select>
            </div><br>
            <button type="submit" class="btn btn-primary">Добавить</button>

        </form>
    </div>


@endsection