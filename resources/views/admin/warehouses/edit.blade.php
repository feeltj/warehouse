@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-institution'></i> Редактировать Cклад</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('warehouses.update', $warehouse->id) }}" method="post" >
            <input type="hidden" name="_method" value="PUT">
            @csrf

            <div class="form-group">
                <label for="name">Название Склада</label>
                <input type="text" name="name" class="form-control" value="{{$warehouse->name}}" placeholder="Название" required>
            </div><br>
            <div class="form-group">
                <label for="name">Регион</label>
                <select class="form-control" name="region_id" id="region">
                    @foreach($regions as $region)
                        <option value="{{$region->id}}" {{ $region->id == $warehouse->region_id ? "selected" : "" }}>{{ $region->name }}</option>
                    @endforeach
                </select>
            </div><br>
            <button type="submit" class="btn btn-primary">Изменить</button>


        </form>
    </div>


@endsection