@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-institution'></i>Добавить Толлинговый Объект</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('tollings.store') }}" method="post" >
            @csrf

            <div class="form-group">
                <label for="name">Название Объекта</label>
                <input type="text" name="name" class="form-control" placeholder="Название" required>
            </div><br>
            <div class="form-group">
                <label for="name">Регион</label>
                <select class="form-control" name="region_id" id="region">
                    @foreach($regions as $region)
                        <option value="{{$region->id}}">{{ $region->name }}</option>
                    @endforeach
                </select>
            </div><br>
        <button type="submit" class="btn btn-primary">Добавить</button>

        </form>
    </div>


@endsection