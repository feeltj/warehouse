@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">

            <h3 class="box-title"><i class='fa fa-user-plus'></i> Добавить пользователя</h3>
            <hr>
            @if($errors->any())
                <div class="alert alert-danger">
                      <ul>
                          @foreach($errors->all() as $error)
                              <li>{{ $error  }}</li>
                              @endforeach
                      </ul>
                </div>
                @endif
            <form action="{{ route('users.store') }}" method="post" >
                @csrf
            <div class="form-group">
                <label>Имя</label>
                <input type="text" name="name" class="form-control" placeholder="Имя" required>
            </div>

            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control" placeholder="Email"  required>
            </div>
                <div class='form-group'>
                    <label>Регион</label>
                    <select class="form-control" name="region_id">
                        @foreach ($regions as $region)

                            <option value="{{ $region->id }}">{{ $region->name }}</option>


                        @endforeach
                    </select>
                </div>
                <div class='form-group'>
                    <label>Отдел</label>
                    <select class="form-control" name="department_id">
                        @foreach ($departments as $department)

                            <option value="{{ $department->id }}" >{{ $department->name }}</option>


                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Телефон</label>
                    <input type="text" class="form-control" placeholder="Телефон" name="phone"> </div>
                <div class="form-group">
                    <label>Адрес</label>
                    <input type="text" class="form-control" placeholder="Адрес проживания" name="address"> </div>
                <div class="form-group">
                    <h5><b>Дать Роль</b></h5>
            <div class='form-group'>
                <select class="form-control" name="roles[]" multiple>
                    @foreach ($roles as $role)

                        <option value="{{ $role->id }}">{{ $role->name }}</option>


                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Пароль">

            </div>

            <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control" placeholder="Повторить Пароль">

            </div>
                    <button type="submit" class="btn btn-primary">Добавить пользователя</button>

                </div>
            </form>
                </div>
            </div>
        </div>
        </div>
    </div>
    @endsection