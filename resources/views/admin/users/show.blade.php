@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">

        <h3><i class='fa fa-user-plus'></i> {{$user->name}}</h3>



            <div class="form-group">
                <label for="name">Имя</label>
                <input type="text" value="{{$user->name}}" class="form-control" id="name"  name="name" disabled>
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" value="{{$user->email}}" class="form-control" id="email"  name="email" disabled>
            </div>

            <div class='form-group'>
                <label>Регион</label>
                <select class="form-control" name="region_id" disabled>
                    @foreach ($regions as $region)

                        <option value="{{ $region->id }}" {{ $user->region_id == $region->id ? "selected" : ""  }}>{{ $region->name }}</option>


                    @endforeach
                </select>
            </div>
            <div class='form-group'>
                <label>Отдел</label>
                <select class="form-control" name="department_id" disabled>
                    @foreach ($departments as $department)

                        <option value="{{ $department->id }}" {{ $user->department_id == $department->id ? "selected" : ""  }}>{{ $department->name }}</option>


                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="phone">Телефон</label>
                <input type="text" value="{{$user->phone}}" class="form-control" id="phone"  name="phone" disabled>
            </div>

            <div class="form-group">
                <label for="address">Адрес</label>
                <input type="text" value="{{$user->address}}" class="form-control" id="address"  name="address" disabled>
            </div>


    </div>

@endsection