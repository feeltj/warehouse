@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">

                <h3><i class='fa fa-user-plus'></i> Редактировать {{$user->name}}</h3>
            <hr>
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error  }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


<form action="{{ route('users.update',$user->id) }}" method="post">
    <input type="hidden" name="_method" value="PUT">
    @csrf
    <div class="form-group">
        <label for="name">Имя</label>
        <input type="text" value="{{$user->name}}" class="form-control" id="name"  name="name" >
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" value="{{$user->email}}" class="form-control" id="email"  name="email" >
    </div>

    <div class='form-group'>
        <label>Регион</label>
        <select class="form-control" name="region_id">
            @foreach ($regions as $region)

                <option value="{{ $region->id }}" {{ $user->region_id == $region->id ? "selected" : ""  }}>{{ $region->name }}</option>


            @endforeach
        </select>
    </div>
    <div class='form-group'>
        <label>Отдел</label>
        <select class="form-control" name="department_id">
            @foreach ($departments as $department)

                <option value="{{ $department->id }}" {{ $user->department_id == $department->id ? "selected" : ""  }}>{{ $department->name }}</option>


            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="phone">Телефон</label>
        <input type="text" value="{{$user->phone}}" class="form-control" id="phone"  name="phone" >
    </div>

    <div class="form-group">
        <label for="address">Адрес</label>
        <input type="text" value="{{$user->address}}" class="form-control" id="address"  name="address" >
    </div>


            <h5><b>Роль</b></h5>

            <div class='form-group'>
                    <select class="form-control" name="roles[]" multiple>
                        @foreach ($roles as $role)

                            <option value="{{ $role->id }}">{{ $role->name }}</option>


                        @endforeach
                    </select>

            </div>

            <div class="form-group">

                <input type="password" name="password" class="form-control" placeholder="Пароль">

            </div>

            <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control" placeholder="Повторить Пароль">

            </div>
<button type="submit" class="btn btn-primary">Редактировать</button>

</form>


        </div>
            </div>
        </div>
    </div>

    @endsection