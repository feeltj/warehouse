@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
            <h3><i class='fa fa-key'></i>Добавить Права Доступа</h3>
            <br>


                    <form action="{{ route('permissions.store') }}" method="post" >
                        @csrf

                        <div class="form-group">

                            <input type="text" name="name" class="form-control" placeholder="Название" required>
                        </div><br>
            @if(!$roles->isEmpty())
            <h4>Привязать Права Доступа к Роли</h4>

            <select class="form-control" name="roles[]" multiple>
                @foreach ($roles as $role)

                    <option value="{{ $role->id }}">{{ $role->name }}</option>


                @endforeach
            </select>
            @endif
            <br>
            {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
                </div>


    @endsection