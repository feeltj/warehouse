@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">

            <h3><i class='fa fa-key'></i> Редактировать {{$permission->name}}</h3>
            <br>
            <form action="{{ route('permissions.update',$permission->id) }}" method="post">
                <input type="hidden" name="_method" value="PUT">
                @csrf
                <div class="form-group">
                    <label for="name">Название Права Доступа</label>
                    <input type="text" value="{{$permission->name}}" class="form-control" id="name"  name="name" >
                </div>
            <br>
                <button type="submit" class="btn btn-primary">Редактировать</button>

            </form>
                </div>


    @endsection