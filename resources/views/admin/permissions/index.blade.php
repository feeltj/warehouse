@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <!-- page content -->
        <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Кол-во Пользователей</span>
                <div class="count">{{ $user}}</div>

            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-desktop"></i> Кол-во оборудования</span>
                <div class="count">{{ $equiptment }}</div>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-desktop"></i> Кол-во складов</span>
                <div class="count">{{ $warehouse }}</div>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-desktop"></i> Кол-во категорий</span>
                <div class="count">{{ $category }}</div>
            </div>



        </div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
            <h2><i class="fa fa-key"></i>Доступные Права Доступа

                <a href="{{ route('users.index') }}" class="btn btn-success pull-right">Пользователи</a>
                <a href="{{ route('roles.index') }}" class="btn btn-primary pull-right">Роли</a></h2>
            <hr>
                    <div class="x_content">

                        <table id="datatable" class="table table-striped table-bordered">

                    <thead>
                    <tr>
                        <th>Права Доступа</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($permissions as $permission)
                        <tr>
                            <td>{{ $permission->name }}</td>
                            <td>
                                <a href="{{ route('permissions.edit', $permission->id) }}" class="btn btn-sm btn-success-outline"  style="margin-right: 3px;"><i class="fa fa-edit"  aria-hidden="true"></i></a>


                                <form action="{{ route('permissions.destroy', $permission->id) }}" method="post" >
                                    {{method_field('DELETE')}}
                                    @csrf
                                    <button type="submit" style="border: 0; background: none;margin-left: 5px;" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
                    <a href="{{ route('permissions.create') }}" class="btn btn-success">Создать права доступа</a>

                </div>

                            </div>

                        </div>

                    </div>

@endsection