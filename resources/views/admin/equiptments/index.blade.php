@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <!-- page content -->
        <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Кол-во Пользователей</span>
                <div class="count">{{ $user}}</div>

            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-desktop"></i> Кол-во оборудования</span>
                <div class="count">{{ $equiptment }}</div>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-desktop"></i> Кол-во складов</span>
                <div class="count">{{ $warehouse }}</div>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-desktop"></i> Кол-во категорий</span>
                <div class="count">{{ $category }}</div>
            </div>



        </div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                    <h2><i class="fa fa-desktop"></i> Список Документов </h2>
                    <hr>

                        <form action="{{ route('equiptments.index') }}" method="get">
                            <div class="input-group">
                                <input type="text" class="form-control" name="s"
                                       placeholder="Поиск"> <span class="input-group-btn" value="{{ isset($s) ? : '' }}">
                                    <button type="submit" class="btn btn-default">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                        </form>

                    <div class="x_content">

                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Наименование</th>
                                <th>Подкатегория</th>
                                <th>Количество</th>
                                <th>Дата</th>
                                <th>Действия</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach ($equiptments as $equiptment)
                                <tr>
                                    <td>{{ $equiptment->id }}</td>
                                    <td> <a href="{{ route('equiptments.show',$equiptment->id) }}">{{$equiptment->name }}</a></td>
                                    @foreach($subcategories as $subcategory)
                                        @if( $equiptment->subcategory_id == $subcategory->id)
                                            <td>{{ $subcategory->name }}</td>


                                            @endif
                                            @endforeach


                                    <td>{{ $equiptment->quantity }}</td>
                                    <?php
                                    setlocale(LC_ALL, 'ru_RU.UTF-8');
                                    ?>
                                    <td>{{  $equiptment->created_at->formatLocalized('%d %B %Y, %I:%M:%S %p') }}</td>
                                    {{-- Retrieve array of roles associated to a user and convert to string --}}
                                    <td>
                                        <a href="{{ route('equiptments.show', $equiptment->id) }}" class="btn btn-success"  style="margin-right: 3px;">Подробнее</a>
                                        <a href="{{ route('equiptments.edit', $equiptment->id) }}" class="btn btn-primary"  style="margin-right: 3px;">Редактировать</a>


                                        <form action="{{ route('equiptments.destroy', $equiptment->id) }}" method="post" class="btn-group inline" >
                                            {{method_field('DELETE')}}
                                            @csrf
                                            <button type="submit" class="btn btn-danger" style="position: relative;bottom:2px">Удалить</button>
                                        </form>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                        {{ $equiptments->links() }}

                    </div>
                    <a href="{{ route('equiptments.create') }}" class="btn btn-success">Создать Документ</a>
                </div>

            </div>

        </div>
    </div>
@endsection