@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-desktop'></i> {{ $equiptment->name }}</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


            <div class="form-group">
                <label for="name">Название</label>

                <select class="form-control" disabled>
                    <option value="{{  $equiptment->id}}" {{ $equiptment->id ? "selected" : ""  }} >{{  $equiptment->name}}</option>
                </select>
            </div><br>
        <div class="form-group">
            <label for="name">Описание</label>
            <textarea name="description" class="form-control" disabled>{{  $equiptment->description}}</textarea>
        </div><br>

        <div class="form-group" >
            <label for="quantity">Количество</label>
            <input type="text" name="quantity" class="form-control" value="{{  $equiptment->quantity}}" placeholder="" disabled="">
        </div>


            <div class="form-group">
                <label for="name">Категория</label>
                <select class="form-control" name="category_id" id="category" disabled>

                    @foreach($categories as $category)

                        <option value="{{ $category->id }}" {{ $equiptment->category_id == $category->id ? "selected" : ""  }} >{{ $category->name }}</option>

                    @endforeach
                </select>
            </div><br>

            <div class="form-group">
                <label for="name">Подкатегория</label>
                <select class="form-control" name="subcategory_id" id="subcategory" disabled>

                    @foreach($subcategories as $subcategory)
                        <option>Не выбрано</option>
                        <option value="{{ $subcategory->id }}" {{ $equiptment->subcategory_id == $subcategory->id ? "selected" : ""  }}>{{ $subcategory->name }}</option>

                    @endforeach
                </select>
            </div><br>
        <div class="form-group">
            <label for="name">Финальная категория</label>
            <select class="form-control" name="finalcategory_id" id="finalcategory" disabled>

                @foreach($finalcategories as $finalcategory)
                    <option>Не выбрано</option>
                    <option value="{{ $finalcategory->id }}" {{ $equiptment->finalcategory_id == $finalcategory->id ? "selected" : ""  }}>{{ $finalcategory->name }}</option>

                @endforeach
            </select>
        </div><br>
        <!--<div class="form-group" id="line_num">
            <label for="name">Линия</label>
            <select class="form-control" name="line_id" id="line" disabled>
                @foreach($lines as $line)
                    <option>Не выбрано</option>
                    <option value="{{ $line->id }}" {{ $equiptment->line_id == $line->id ? "selected" : ""  }}>{{ $line->value }}</option>
                @endforeach
            </select>
        </div><br>

            <div class="form-group">
                <label for="name">Состояние</label>
                <select class="form-control" name="equiptstatus_id" id="equiptstatus"  disabled>
                    @foreach($equiptstatuses as $equiptstatus)
                        <option value="{{ $equiptstatus->id }}" {{ $equiptment->equiptstatus_id == $equiptstatus->id ? "selected" : ""  }}>{{ $equiptstatus->name }}</option>
                    @endforeach
                </select>
            </div><br>

            <div class="form-group">
                <label for="name">Материально ответственный</label>
                <select class="form-control" name="user_id" id="user" disabled>
                    @foreach($users as $user)
                        <option value="{{ $user->id }}"  {{ $equiptment->user_id == $user->id ? "selected" : ""  }} >{{ $user->name }}</option>
                    @endforeach
                </select>
            </div><br>

            <div class="form-group">
                <label for="name">Регионы</label>
                <select class="form-control" name="region_id" id="region" disabled>
                    @foreach($regions as $region)
                        <option value="{{ $region->id }}" {{ $equiptment->region_id == $region->id ? "selected" : ""  }} >{{ $region->name }}</option>
                    @endforeach
                </select>
            </div><br>

            <div class="form-group">
                <label for="name">Склады</label>
                <select class="form-control" name="warehouse_id" id="warehouse" disabled>
                    @foreach($warehouses as $warehouse)
                        <option value="{{ $warehouse->id }}" {{ $equiptment->warehouse_id == $warehouse->id ? "selected" : ""  }} readonly="">{{ $warehouse->name }}</option>
                    @endforeach
                </select>
            </div><br>-->
    </div>

@endsection