@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-desktop'></i> Редактировать Оборудование</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('equiptments.update',$equiptment->id) }}" method="post" >
            <input type="hidden" name="_method" value="PUT">
            @csrf

            <div class="form-group">
                <label for="name">Название</label>
                <input type="text" name="name" value="{{  $equiptment->name}}" class="form-control" placeholder="" required>
            </div><br>
            <div class="form-group">
                <label for="name">Описание</label>
                <textarea name="description" class="form-control" required>{{  $equiptment->description}}</textarea>
            </div><br>
            <div class="form-group">
                <label for="quantity">Количество</label>
                <input type="text" name="quantity" class="form-control" value="{{  $equiptment->quantity}}" placeholder="" required>
            </div>

            <div class="form-group">
                <label for="name">Категория</label>
                <select class="form-control" name="category_id" id="category">

                    @foreach($categories as $category)

                        <option value="{{ $category->id }}" {{ $equiptment->category_id == $category->id ? "selected" : ""  }}>{{ $category->name }}</option>

                    @endforeach
                </select>
            </div><br>

            <div class="form-group">
                <label for="name">Подкатегория</label>
                <select class="form-control" name="subcategory_id" id="subcategory">

                    @foreach($subcategories as $subcategory)
                        @if($equiptment->subcategory_id == $subcategory->id)
                        <option value="{{ $subcategory->id }}" {{ $equiptment->subcategory_id == $subcategory->id ? "selected" : ""  }}>{{ $subcategory->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div><br>
            <div class="form-group">
                <label for="name">Финальная категория</label>
                <select class="form-control" name="finalcategory_id" id="finalcategory">

                    @foreach($finalcategories as $finalcategory)
                        @if($equiptment->finalcategory_id == $finalcategory->id)
                        <option value="{{ $finalcategory->id }}" {{ $equiptment->finalcategory_id == $finalcategory->id ? "selected" : ""  }}>{{ $finalcategory->name }}</option>

                        @endif
                    @endforeach
                </select>
            </div><br>

            <button type="submit" class="btn btn-primary">Изменить</button>

        </form>
    </div>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $('#category').select2({
            closeOnSelect: true
        });

        $('#subcategory').select2({
            closeOnSelect: true
        });
        $('#finalcategory').select2({
            closeOnSelect: true
        });

    </script>
    <script>
        $('#category').on('change', function(e){
            //console.log(e);
            var cat_id = e.target.value;
            console.log($('#category').val());

            //if(cat_id == 10)
            //{
            //$('#line_num').show();
            $.get('{{ url('admin/information') }}/create?category_id=' + cat_id, function(data) {
                console.log(data);

                $('#line').empty();
                $('#subcategory').empty();
                $('#finalcategory').empty();
                $.each(data, function(index,subCatObj){
                    $('#line').removeAttr('disabled');
                    //console.log(subCatObj.name);
                    $('#line').append('<option value="'+subCatObj.id+'">'+subCatObj.value+'</option>')
                });

            });
            //}
            //else {
            //$('#line_num').hide();
            $.get('{{ url('admin/information') }}/create/ajax-state?category_id=' + cat_id, function (data) {
                //console.log(data);
                $('#subcategory').empty();

                $.each(data, function (index, subCatObj) {
                   // $('#line').prop('disabled', true);
                    //console.log(subCatObj.name);
                    $('#subcategory').append('<option value="' + subCatObj.id + '">' + subCatObj.name + '</option>')
                });

            });
            //}
        });
        $('#region').on('change', function(e){
            console.log(e);
            var region_id = e.target.value;

            $.get('{{ url('admin/information') }}/create/ajax?region_id=' + region_id, function(data) {
                //console.log(data);
                $('#warehouse').empty();
                $.each(data, function(index,subCatObj){
                    //console.log(subCatObj.name);
                    $('#warehouse').append('<option value="'+subCatObj.id+'">'+subCatObj.name+'</option>')
                });

            });
        });
        $('#subcategory').on('change', function(e){
            var subcat_id = e.target.value;

            $.get('{{ url('admin/information') }}/final?subcategory_id=' + subcat_id, function (data) {
                //console.log(data);
                $('#finalcategory').empty();

                $.each(data, function (index, subCatObj) {
                    //$('#line').prop('disabled', true);
                    //console.log(subCatObj.name);
                    $('#finalcategory').append('<option value="' + subCatObj.id + '">' + subCatObj.name + '</option>')
                });

            });

        });
    </script>

@endsection