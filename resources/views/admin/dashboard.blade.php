@extends('layouts.app_header_admin')
@section('content')
@section('styles')
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap.min.css" rel="stylesheet" rel="stylesheet">
    @endsection
    <!-- Content Wrapper. Contains page content -->
    <!-- /top navigation -->
    <div class="right_col" role="main">
        <!-- page content -->
        <div class="row tile_count">
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Кол-во Пользователей</span>
                <div class="count">{{ $user}}</div>

            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-desktop"></i> Кол-во оборудования</span>
                <div class="count">{{ $equiptment }}</div>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-desktop"></i> Кол-во складов</span>
                <div class="count">{{ $warehouse }}</div>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-desktop"></i> Кол-во категорий</span>
                <div class="count">{{ $category }}</div>
            </div>


</div>
        <div class="row tile_count">

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-balance-scale"></i> Кол-во Регионов</span>
                <div class="count">{{ $region }}</div>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-building"></i> Кол-во Отделов</span>
                <div class="count">{{ $department }}</div>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-car"></i> Кол-во Толлинговых объектов</span>
                <div class="count">{{ $tolling }}</div>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-car"></i> Кол-во Линий</span>
                <div class="count">{{ $line }}</div>
            </div>

        </div>
            <!-- /top tiles -->

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

                <table class="table table-striped table-bordered" id="example" class="display" style="width:100%">
                    <thead>
                    <tr>
                    <th>
                        Названия Оборудования
                    </th>
                    <th>
                        Материально Ответсвенные
                    </th>
                        <th>
                            Регион
                        </th>
                    <th>
                        Количество Оборудований
                    </th>
                    </tr>
                    </thead>


                    <tbody>
                    @foreach($stock_amount as $amount)

                        @foreach($users as $user)
                         @foreach($regions as $region)
                            @if( $amount->user_id == $user->id && $amount->region_id == $region->id)

                                <tr>
                                <td>{{ $amount->name }}</td>
                                <td>{{ $user->name }}</td>
                                    <td>{{ $region->name }}</td>
                                <td>{{ $amount->equiptment_id}}</td>
                                </tr>


                            @endif
                             @endforeach
                        @endforeach
                    @endforeach
                    </tbody>

                </table>

    </div>
   @section('javascripts')
       <script src="
https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
       <script>
           $(document).ready(function() {
               var table = $('#example').DataTable( {
                   lengthChange: false,
                   buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
               } );

               table.buttons().container()
                   .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
           } );
    </script>
       @endsection
@endsection