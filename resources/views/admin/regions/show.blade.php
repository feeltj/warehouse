@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class="fa fa-balance-scale"></i> {{ $region->name }}</h3>
        <br>



            <div class="form-group">
                <label for="name">Название Региона</label>
                <input type="text" name="name" value="{{ $region->name }}" class="form-control" placeholder="Название региона" disabled>
            </div><br>

    </div>


@endsection