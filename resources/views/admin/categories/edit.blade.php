@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-folder-o'></i> Редактировать Категорию</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('categories.update',$category->id) }}" method="post" >
            <input type="hidden" name="_method" value="PUT">
            @csrf

            <div class="form-group">
                <label for="name">Название Категории</label>
                <input type="text" name="name" value="{{ $category->name }}" class="form-control" placeholder="Название категории" required>
            </div><br>
            <button type="submit" class="btn btn-primary">Изменить</button>

        </form>
    </div>


@endsection