@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-car'></i> {{ $category->name }}</h3>
        <br>



            <div class="form-group">
                <label for="name">Название Категории</label>
                <input type="text" name="name" value="{{ $category->name }}" class="form-control" placeholder="Название категории" disabled>
            </div><br>

    </div>


@endsection