@extends('layouts.app_header_admin')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="right_col" role="main">
        <h3><i class='fa fa-folder-o'></i> Добавить Категорию</h3>
        <br>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error  }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('categories.store') }}" method="post" >
            @csrf

            <div class="form-group">
                <label for="name">Название Категории</label>
                <input type="text" name="name" class="form-control" placeholder="Название категории" required>
            </div><br>
            <button type="submit" class="btn btn-primary">Добавить</button>

        </form>
    </div>


@endsection