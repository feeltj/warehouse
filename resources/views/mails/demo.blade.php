<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
</head>
<body>
Здравствуйте <i>{{ $demo->receiver }}</i>,
<p>Это email для получения вам информации что вы материально ответсвенный за оборудования.</p>

<p><u>Толлинговые оборудования:</u></p>

<div>
    <p><b>Специалист:</b>&nbsp;{{ $demo->demo_one }}</p>
    <p><b>Названия Оборудования:</b>&nbsp;{{ $demo->demo_two }}</p>
    <p><b>Общее Количество Оборудований:</b>&nbsp;{{ $demo->demo_three }}</p>
</div>

<p><u>Вы получили данные просим вас утвердить о том что вы приняли оборудование: </u></p>
<p><a href="{{url('/home')}}">Потвердите ответственность</a></p>
Спасибо с уважением,
<br/>
<i>{{ $demo->sender }}</i>
</body>
</html>
