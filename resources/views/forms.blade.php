@extends('layouts.app')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Advanced Form Elements
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="#">Forms</a></li>
            <li class="breadcrumb-item active">Advanced Elements</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Select Elements</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <label>Minimal</label>
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">Alabama</option>
                                <option>Alaska</option>
                                <option>California</option>
                                <option>Delaware</option>
                                <option>Tennessee</option>
                                <option>Texas</option>
                                <option>Washington</option>
                            </select>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>Disabled</label>
                            <select class="form-control select2" disabled="disabled" style="width: 100%;">
                                <option selected="selected">Alabama</option>
                                <option>Alaska</option>
                                <option>California</option>
                                <option>Delaware</option>
                                <option>Tennessee</option>
                                <option>Texas</option>
                                <option>Washington</option>
                            </select>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6 col-12">
                        <div class="form-group">
                            <label>Multiple</label>
                            <select class="form-control select2" multiple="multiple" data-placeholder="Select a State"
                                    style="width: 100%;">
                                <option>Alabama</option>
                                <option>Alaska</option>
                                <option>California</option>
                                <option>Delaware</option>
                                <option>Tennessee</option>
                                <option>Texas</option>
                                <option>Washington</option>
                            </select>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label>Disabled Result</label>
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">Alabama</option>
                                <option>Alaska</option>
                                <option disabled="disabled">California (disabled)</option>
                                <option>Delaware</option>
                                <option>Tennessee</option>
                                <option>Texas</option>
                                <option>Washington</option>
                            </select>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <i class="fa fa-check-square-o text-black"></i>

                        <h3 class="box-title">Basic Checkbox</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="demo-checkbox">
                            <input type="checkbox" id="basic_checkbox_1" checked />
                            <label for="basic_checkbox_1">Default</label>
                            <input type="checkbox" id="basic_checkbox_2" class="filled-in" checked />
                            <label for="basic_checkbox_2">Filled In</label>
                            <input type="checkbox" id="basic_checkbox_3" checked disabled />
                            <label for="basic_checkbox_3">Default - Disabled</label>
                            <input type="checkbox" id="basic_checkbox_4" class="filled-in" checked disabled />
                            <label for="basic_checkbox_4">Filled In - Disabled</label>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <i class="fa fa-check-square-o text-black"></i>

                        <h3 class="box-title">Basic Checkbox Design Colors</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="demo-checkbox">
                            <input type="checkbox" id="md_checkbox_1" class="chk-col-red" checked />
                            <label for="md_checkbox_1">Red</label>
                            <input type="checkbox" id="md_checkbox_3" class="chk-col-purple" checked />
                            <label for="md_checkbox_3">Purple</label>
                            <input type="checkbox" id="md_checkbox_4" class="chk-col-deep-purple" checked />
                            <label for="md_checkbox_4">Deep Purple</label>
                            <input type="checkbox" id="md_checkbox_6" class="chk-col-blue" checked />
                            <label for="md_checkbox_6">Blue</label>
                            <input type="checkbox" id="md_checkbox_7" class="chk-col-light-blue" checked />
                            <label for="md_checkbox_7">Light Blue</label>
                            <input type="checkbox" id="md_checkbox_9" class="chk-col-teal" checked />
                            <label for="md_checkbox_9">Teal</label>
                            <input type="checkbox" id="md_checkbox_10" class="chk-col-green" checked />
                            <label for="md_checkbox_10">Green</label>
                            <input type="checkbox" id="md_checkbox_13" class="chk-col-yellow" checked />
                            <label for="md_checkbox_13">Yellow</label>
                            <input type="checkbox" id="md_checkbox_15" class="chk-col-orange" checked />
                            <label for="md_checkbox_15">Orange</label>
                            <input type="checkbox" id="md_checkbox_16" class="chk-col-deep-orange" checked />
                            <label for="md_checkbox_16">Deep Orange</label>
                            <input type="checkbox" id="md_checkbox_17" class="chk-col-maroon" checked />
                            <label for="md_checkbox_17">Maroon</label>
                            <input type="checkbox" id="md_checkbox_18" class="chk-col-grey" checked />
                            <label for="md_checkbox_18">Grey</label>
                            <input type="checkbox" id="md_checkbox_19" class="chk-col-navy" checked />
                            <label for="md_checkbox_19">Navy</label>
                            <input type="checkbox" id="md_checkbox_20" class="chk-col-black" checked />
                            <label for="md_checkbox_20">Black</label>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <i class="fa fa-check-square-o text-black"></i>

                        <h3 class="box-title">Basic Checkbox Design Colors with Filled In</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="demo-checkbox">
                            <input type="checkbox" id="md_checkbox_21" class="filled-in chk-col-red" checked />
                            <label for="md_checkbox_21">Red</label>
                            <input type="checkbox" id="md_checkbox_23" class="filled-in chk-col-purple" checked />
                            <label for="md_checkbox_23">Purple</label>
                            <input type="checkbox" id="md_checkbox_24" class="filled-in chk-col-deep-purple" checked />
                            <label for="md_checkbox_24">Deep Purple</label>
                            <input type="checkbox" id="md_checkbox_26" class="filled-in chk-col-blue" checked />
                            <label for="md_checkbox_26">Blue</label>
                            <input type="checkbox" id="md_checkbox_27" class="filled-in chk-col-light-blue" checked />
                            <label for="md_checkbox_27">Light Blue</label>
                            <input type="checkbox" id="md_checkbox_29" class="filled-in chk-col-teal" checked />
                            <label for="md_checkbox_29">Teal</label>
                            <input type="checkbox" id="md_checkbox_30" class="filled-in chk-col-green" checked />
                            <label for="md_checkbox_30">Green</label>
                            <input type="checkbox" id="md_checkbox_33" class="filled-in chk-col-yellow" checked />
                            <label for="md_checkbox_33">Yellow</label>
                            <input type="checkbox" id="md_checkbox_35" class="filled-in chk-col-orange" checked />
                            <label for="md_checkbox_35">Orange</label>
                            <input type="checkbox" id="md_checkbox_36" class="filled-in chk-col-deep-orange" checked />
                            <label for="md_checkbox_36">Deep Orange</label>
                            <input type="checkbox" id="md_checkbox_37" class="filled-in chk-col-maroon" checked />
                            <label for="md_checkbox_37">Maroon</label>
                            <input type="checkbox" id="md_checkbox_38" class="filled-in chk-col-grey" checked />
                            <label for="md_checkbox_38">Grey</label>
                            <input type="checkbox" id="md_checkbox_39" class="filled-in chk-col-navy" checked />
                            <label for="md_checkbox_39">Navy</label>
                            <input type="checkbox" id="md_checkbox_40" class="filled-in chk-col-black" checked />
                            <label for="md_checkbox_40">Black</label>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <i class="fa fa-check-circle text-black"></i>

                        <h3 class="box-title">Basic Radio Button</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="demo-radio-button">
                            <input name="group1" type="radio" id="radio_1" checked />
                            <label for="radio_1">Radio - 1</label>
                            <input name="group1" type="radio" id="radio_2" />
                            <label for="radio_2">Radio - 2</label>
                            <input name="group1" type="radio" class="with-gap" id="radio_3" />
                            <label for="radio_3">Radio - With Gap</label>
                            <input name="group1" type="radio" id="radio_4" class="with-gap" />
                            <label for="radio_4">Radio - With Gap</label>
                            <input name="group2" type="radio" id="radio_5" checked disabled />
                            <label for="radio_5">Radio - Disabled</label>
                            <input name="group3" type="radio" id="radio_6" class="with-gap" checked disabled />
                            <label for="radio_6">Radio - Disabled</label>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <i class="fa fa-check-circle text-black"></i>

                        <h3 class="box-title">Basic Radio Button Design Colors</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="demo-radio-button">
                            <input name="group4" type="radio" id="radio_7" class="radio-col-red" checked />
                            <label for="radio_7">Red</label>
                            <input name="group4" type="radio" id="radio_9" class="radio-col-purple" />
                            <label for="radio_9">Purple</label>
                            <input name="group4" type="radio" id="radio_10" class="radio-col-deep-purple" />
                            <label for="radio_10">Deep Purple</label>
                            <input name="group4" type="radio" id="radio_12" class="radio-col-blue" />
                            <label for="radio_12">Blue</label>
                            <input name="group4" type="radio" id="radio_13" class="radio-col-light-blue" />
                            <label for="radio_13">Light Blue</label>
                            <input name="group4" type="radio" id="radio_15" class="radio-col-teal" />
                            <label for="radio_15">Teal</label>
                            <input name="group4" type="radio" id="radio_16" class="radio-col-green" />
                            <label for="radio_16">Green</label>
                            <input name="group4" type="radio" id="radio_19" class="radio-col-yellow" />
                            <label for="radio_19">Yellow</label>
                            <input name="group4" type="radio" id="radio_21" class="radio-col-orange" />
                            <label for="radio_21">Orange</label>
                            <input name="group4" type="radio" id="radio_22" class="radio-col-deep-orange" />
                            <label for="radio_22">Deep Orange</label>
                            <input name="group4" type="radio" id="radio_23" class="radio-col-maroon" />
                            <label for="radio_23">Maroon</label>
                            <input name="group4" type="radio" id="radio_24" class="radio-col-grey" />
                            <label for="radio_24">Grey</label>
                            <input name="group4" type="radio" id="radio_25" class="radio-col-navy" />
                            <label for="radio_25">Navy</label>
                            <input name="group4" type="radio" id="radio_26" class="radio-col-black" />
                            <label for="radio_26">Black</label>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <i class="fa fa-check-circle-o text-black"></i>

                        <h3 class="box-title">Basic Radio Button Design Colors with Outline</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="demo-radio-button">
                            <input name="group5" type="radio" id="radio_30" class="with-gap radio-col-red" checked />
                            <label for="radio_30">Red</label>
                            <input name="group5" type="radio" id="radio_32" class="with-gap radio-col-purple" />
                            <label for="radio_32">Purple</label>
                            <input name="group5" type="radio" id="radio_33" class="with-gap radio-col-deep-purple" />
                            <label for="radio_33">Deep Purple</label>
                            <input name="group5" type="radio" id="radio_35" class="with-gap radio-col-blue" />
                            <label for="radio_35">Blue</label>
                            <input name="group5" type="radio" id="radio_36" class="with-gap radio-col-light-blue" />
                            <label for="radio_36">LIight Blue</label>
                            <input name="group5" type="radio" id="radio_38" class="with-gap radio-col-teal" />
                            <label for="radio_38">Teal</label>
                            <input name="group5" type="radio" id="radio_39" class="with-gap radio-col-green" />
                            <label for="radio_39">Green</label>
                            <input name="group5" type="radio" id="radio_42" class="with-gap radio-col-yellow" />
                            <label for="radio_42">Yellow</label>
                            <input name="group5" type="radio" id="radio_44" class="with-gap radio-col-orange" />
                            <label for="radio_44">Orange</label>
                            <input name="group5" type="radio" id="radio_45" class="with-gap radio-col-deep-orange" />
                            <label for="radio_45">Deep Orange</label>
                            <input name="group5" type="radio" id="radio_46" class="with-gap radio-col-maroon" />
                            <label for="radio_46">Maroon</label>
                            <input name="group5" type="radio" id="radio_47" class="with-gap radio-col-grey" />
                            <label for="radio_47">Grey</label>
                            <input name="group5" type="radio" id="radio_48" class="with-gap radio-col-navy" />
                            <label for="radio_48">Navy</label>
                            <input name="group5" type="radio" id="radio_49" class="with-gap radio-col-black" />
                            <label for="radio_49">Black</label>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6 col-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Input masks</h3>
                    </div>
                    <div class="box-body">
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Date masks:</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->

                        <!-- Date mm/dd/yyyy -->
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->

                        <!-- phone mask -->
                        <div class="form-group">
                            <label>US phone mask:</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input type="text" class="form-control" data-inputmask="'mask':[ '(999) 999-9999']" data-mask>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->

                        <!-- phone mask -->
                        <div class="form-group">
                            <label>Intl US phone mask:</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input type="text" class="form-control"
                                       data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->

                        <!-- IP mask -->
                        <div class="form-group">
                            <label>IP mask:</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                </div>
                                <input type="text" class="form-control" data-inputmask="'alias': 'ip'" data-mask>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Color & Time Picker</h3>
                    </div>
                    <div class="box-body">
                        <!-- Color Picker -->
                        <div class="form-group">
                            <label>Color picker:</label>
                            <input type="text" class="form-control my-colorpicker1">
                        </div>
                        <!-- /.form group -->

                        <!-- Color Picker -->
                        <div class="form-group">
                            <label>Color picker with addon:</label>

                            <div class="input-group my-colorpicker2">
                                <input type="text" class="form-control">

                                <div class="input-group-addon">
                                    <i class="ion ion-paintbucket"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->

                        <!-- time Picker -->
                        <div class="bootstrap-timepicker">
                            <div class="form-group">
                                <label>Time picker:</label>

                                <div class="input-group">
                                    <input type="text" class="form-control timepicker">

                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col (left) -->
            <div class="col-lg-6 col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Date picker</h3>
                    </div>
                    <div class="box-body">
                        <!-- Date -->
                        <div class="form-group">
                            <label>Date:</label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->

                        <!-- Date range -->
                        <div class="form-group">
                            <label>Date range:</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="reservation">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->

                        <!-- Date and time range -->
                        <div class="form-group">
                            <label>Date and time range:</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="reservationtime">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->

                        <!-- Date and time range -->
                        <div class="form-group">
                            <label>Date range button:</label>

                            <div class="input-group">
                                <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Date range picker
                    </span>
                                    <i class="fa fa-caret-down"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.form group -->

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- iCheck -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">iCheck - Checkbox &amp; Radio Inputs</h3>
                    </div>
                    <div class="box-body">
                        <!-- Minimal style -->

                        <!-- checkbox -->
                        <div class="form-group ichack-input">
                            <label>
                                <input type="checkbox" class="minimal" checked>
                            </label>
                            <label>
                                <input type="checkbox" class="minimal">
                            </label>
                            <label>
                                <input type="checkbox" class="minimal" disabled>
                                Minimal skin checkbox
                            </label>
                        </div>

                        <!-- radio -->
                        <div class="form-group ichack-input">
                            <label>
                                <input type="radio" name="r1" class="minimal" checked>
                            </label>
                            <label>
                                <input type="radio" name="r1" class="minimal">
                            </label>
                            <label>
                                <input type="radio" name="r1" class="minimal" disabled>
                                Minimal skin radio
                            </label>
                        </div>

                        <!-- Minimal red style -->

                        <!-- checkbox -->
                        <div class="form-group ichack-input">
                            <label>
                                <input type="checkbox" class="minimal-red" checked>
                            </label>
                            <label>
                                <input type="checkbox" class="minimal-red">
                            </label>
                            <label>
                                <input type="checkbox" class="minimal-red" disabled>
                                Minimal red skin checkbox
                            </label>
                        </div>

                        <!-- radio -->
                        <div class="form-group ichack-input">
                            <label>
                                <input type="radio" name="r2" class="minimal-red" checked>
                            </label>
                            <label>
                                <input type="radio" name="r2" class="minimal-red">
                            </label>
                            <label>
                                <input type="radio" name="r2" class="minimal-red" disabled>
                                Minimal red skin radio
                            </label>
                        </div>

                        <!-- Minimal red style -->

                        <!-- checkbox -->
                        <div class="form-group ichack-input">
                            <label>
                                <input type="checkbox" class="flat-red" checked>
                            </label>
                            <label>
                                <input type="checkbox" class="flat-red">
                            </label>
                            <label>
                                <input type="checkbox" class="flat-red" disabled>
                                Flat green skin checkbox
                            </label>
                        </div>

                        <!-- radio -->
                        <div class="form-group ichack-input">
                            <label>
                                <input type="radio" name="r3" class="flat-red" checked>
                            </label>
                            <label>
                                <input type="radio" name="r3" class="flat-red">
                            </label>
                            <label>
                                <input type="radio" name="r3" class="flat-red" disabled>
                                Flat green skin radio
                            </label>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col (right) -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)">FAQ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.multipurposethemes.com/product/minimal-pro-responsive-bootstrap-4-admin-dashboard-template-webapp-template/">Purchase Now</a>
            </li>
        </ul>
    </div>
    &copy; 2017 <a href="https://www.multipurposethemes.com/">Multi-Purpose Themes</a>. All Rights Reserved.
</footer>
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-light">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-cog fa-spin"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Admin Birthday</h4>

                            <p>Will be July 24th</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-user bg-yellow"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Jhone Updated His Profile</h4>

                            <p>New Email : jhone_doe@demo.com</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Disha Joined Mailing List</h4>

                            <p>disha@demo.com</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="menu-icon fa fa-file-code-o bg-green"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Code Change</h4>

                            <p>Execution time 15 Days</p>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class="control-sidebar-menu">
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Web Design
                            <span class="label label-danger pull-right">40%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-danger" style="width: 40%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Update Data
                            <span class="label label-success pull-right">75%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-success" style="width: 75%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Order Process
                            <span class="label label-warning pull-right">89%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-warning" style="width: 89%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <h4 class="control-sidebar-subheading">
                            Development
                            <span class="label label-primary pull-right">72%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-primary" style="width: 72%"></div>
                        </div>
                    </a>
                </li>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
                <h3 class="control-sidebar-heading">General Settings</h3>

                <div class="form-group">
                    <input type="checkbox" id="report_panel" class="chk-col-grey" >
                    <label for="report_panel" class="control-sidebar-subheading ">Report panel usage</label>

                    <p>
                        general settings information
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <input type="checkbox" id="allow_mail" class="chk-col-grey" >
                    <label for="allow_mail" class="control-sidebar-subheading ">Mail redirect</label>

                    <p>
                        Other sets of options are available
                    </p>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <input type="checkbox" id="expose_author" class="chk-col-grey" >
                    <label for="expose_author" class="control-sidebar-subheading ">Expose author name</label>

                    <p>
                        Allow the user to show his name in blog posts
                    </p>
                </div>
                <!-- /.form-group -->

                <h3 class="control-sidebar-heading">Chat Settings</h3>

                <div class="form-group">
                    <input type="checkbox" id="show_me_online" class="chk-col-grey" >
                    <label for="show_me_online" class="control-sidebar-subheading ">Show me as online</label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <input type="checkbox" id="off_notifications" class="chk-col-grey" >
                    <label for="off_notifications" class="control-sidebar-subheading ">Turn off notifications</label>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        <a href="javascript:void(0)" class="text-red margin-r-5"><i class="fa fa-trash-o"></i></a>
                        Delete chat history
                    </label>
                </div>
                <!-- /.form-group -->
            </form>
        </div>
        <!-- /.tab-pane -->
    </div>
</aside>
<!-- /.control-sidebar -->

<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
@endsection